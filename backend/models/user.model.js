var mongoose = require( 'mongoose') ; 
var bcrypt  = require('bcryptjs'); 
const Schema = mongoose.Schema;



var userShema = new Schema({
    prenom : {type:String , required : true , max :100},
    nom : {type:String , required : true , max :100},
    sexe : {type:String , max :100},
    email : {type : String, required : true , max :100},
    password : {type : String, required : true , max :100},
    telephone : {type : String, max :100},
    adresse : {type : String,max :100}
})

userShema.pre('save', function(next) {

    if (!this.isModified('password'))  {
      return next();
    }
   //Generate Salt Value
   bcrypt.genSalt(10, (err, salt) => {
     if (err) {
       return next(err);
     }
     //Use this salt value to hash password
     bcrypt.hash(this.password, salt, (err, hash) => {
       if (err) {
         return next(err);
       }
       this.password = hash;
       next();
     });
   });
});

userShema.methods.isPasswordMatch = function(plainPassword, hashed, callback) {
    bcrypt.compare(plainPassword, hashed, (err, isMatch) => {
      if (err) {
        return callback(err);
      }
      callback(null, isMatch);
    });
  }

userShema.index({id : 1});
let User = mongoose.model("User",userShema);
User.createIndexes();

module.exports =  User ;
 