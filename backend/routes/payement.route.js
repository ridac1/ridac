const express = require("express");
const bodyParser = require("body-parser");
const payementRouter = express.Router();
const { resolve } = require("path");
const env = require("dotenv").config({ path: "./.env" });
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
//const port = process.env.PORT || 4242;

// Setup useful middleware.
/*payementRouter.use(
  bodyParser.json({
    // We need the raw body to verify webhook signatures.
    // Let's compute it only when hitting the Stripe webhook endpoint.
    verify: function(req, res, buf) {
      if (req.originalUrl.startsWith("/webhook")) {
        req.rawBody = buf.toString();
      }
    }
  })
);
payementRouter.use(bodyParser.urlencoded({ extended: true }));

payementRouter.use(express.static("../../client"));
payementRouter.use(express.json());*/

// Render the checkout page
payementRouter.get("/", (req, res) => {
  const path = resolve("./client/index.html");
  res.sendFile(path);
});

payementRouter.get("/public-key", (req, res) => {
  res.send({ publicKey: process.env.STRIPE_PUBLISHABLE_KEY });
});

const calculateOrderAmount = items => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client
  return 1999;
};

payementRouter.post("/payment_intents", async (req, res) => {
  let { currency, items } = req.body;
  try {
    const paymentIntent = await stripe.paymentIntents.create({
      amount: calculateOrderAmount(items),
      currency
    });
    return res.status(200).json(paymentIntent);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

//Appliquer la charge
payementRouter.post("/payment_charge", async (req, res) => {
  let token = req.body.cardTocken;
  console.log("requete body :", req.body);
  console.log("Le token envoyé est : ", token);
  //console.log("Le montant envoyé est : ", montant);
  let acc = req.body.devise;
  let mnt;
  if (acc==='xof'){
    mnt = req.body.montant;
  }
  else {
    mnt = req.body.montant*100;
  }
  try {
    const charge = await stripe.charges.create({
      amount: mnt,
      currency: req.body.devise,
      description: 'Virement effectué par '+req.body.name+' '+req.body.prenom,
      source: token.id,
    });
    return res.status(200).json({message: "Payement effectué avec succès"});
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
});

// A webhook to receive events sent from Stripe
// You can listen for specific events
// This webhook endpoint is listening for a payment_intent.succeeded event
payementRouter.post("/webhook", async (req, res) => {
  // Check if webhook signing is configured.
  if (process.env.STRIPE_WEBHOOK_SECRET) {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;
    let signature = req.headers["stripe-signature"];
    try {
      event = stripe.webhooks.constructEvent(
        req.rawBody,
        signature,
        process.env.STRIPE_WEBHOOK_SECRET
      );
    } catch (err) {
      console.log(`⚠️  Webhook signature verification failed.`);
      return res.sendStatus(400);
    }
    data = event.data;
    eventType = event.type;
  } else {
    // Webhook signing is recommended, but if the secret is not configured in `config.js`,
    // we can retrieve the event data directly from the request body.
    data = req.body.data;
    eventType = req.body.type;
  }

  if (eventType === "payment_intent.succeeded") {
    console.log("💰Your user provided payment details!");
    // Fulfill any orders or e-mail receipts
    res.sendStatus(200);
  }
});

//payementRouter.listen(port, () => console.log(`Listening on port ${port}`));
module.exports =  payementRouter;
