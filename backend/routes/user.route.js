var express = require('express');
var User = require('../models/user.model');
var jwt = require('jsonwebtoken');
var multer = require('multer');
var passport = require("passport");
var config = require("../utils/config");
var nodemailer = require('nodemailer');

const userRouter = express.Router();

//Envoie de mail
const transporter = nodemailer.createTransport({

  host: 'smtp.gmail.com',
  provider: 'gmail',
  port: 465,
  secure: true,
  auth: {
    user: 'zz3.instazz@gmail.com', // Enter here email address from which you want to send emails
    pass: 'instaZZ3' // Enter here password for email account from which you want to send emails
  },
  tls: {
  rejectUnauthorized: false
  }
});

//Fonction pour logout
userRouter.get('/', (req, res) => {
  res.send("Hello Friqya Developpers");
  res.end();
});

userRouter.post('/register', (req, res, next) => {
  let newUser = new User({
    prenom: req.body.prenom,
    nom: req.body.nom,
    sexe: req.body.sexe,
    email: req.body.email,
    password: req.body.password,
    telephone: req.body.telephone,
    adresse: req.body.adresse
  });
  //pour lancer le gmail
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'zz3.instazz@gmail.com',
      pass: 'instaZZ3'
    }
  });

  var mailOptions = {
    from: 'blazz3.instazz@gmail.combla',
    to: req.body.email,
    subject: "Création de compte",
    text: "bonjour",
    html: '<b>' + "Bonjour votre compte a été créé avec succès !" + '</b>'
  };

  newUser.save((err, user) => {
    if (err) {
      return res.send({
        success: false,
        message: 'Erreur pour stocker User'
      });
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      res.send({
        success: true,
        message: 'Compte utilisateur créé !'
      });
    });
    transporter.close();
  });
});


//Login
userRouter.post('/auth', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const query = { email }
  //verification que l'email existe
  User.findOne(query, (err, user) => {
    if (err) {
      return res.send({
        success: false,
        message: "Erreur"
      });
    }

    //No User match the search condition
    if (!user) {
      return res.send({
        success: false,
        message: "Email n'existe pas"
      });
    }

    //Check if the password is correct
    user.isPasswordMatch(password, user.password, (err, isMatch) => {

      //Invalid password
      if (!isMatch) {
        return res.send({
          success: false,
          message: 'Mot de passe est invalide'
        });
      }

      const ONE_WEEK = 604800; //Token validtity in seconds

      const token = jwt.sign({ user }, process.env.SECRET, { expiresIn: ONE_WEEK });
      let returnUser = {
        nom: user.nom,
        nom: user.nom,
        email: user.email,
        id: user._id,
        online: true
      }

      //Send the response back
      return res.send({
        success: true,
        message: 'Utilisateur connecté',
        user,
        token
      });
    });
  });
});

//Récuperation de tous les utilisateurs
userRouter.get('/donateurs', (req, res, next) => {
  User.find()
    .then(users => res.status(200).json(users))
    .catch(error => res.status(400).json({ error }));
});

//Nous contacter
userRouter.post('/contact', function (req, res) {

  let senderName = req.body.contactFormName;
  let senderEmail = req.body.contactFormEmail;
  let messageSubject = req.body.contactFormSubjects;
  let messageText = req.body.contactFormMessage;
  let copyToSender = req.body.contactFormCopy;

  let mailOptions = {
    to: ['friqyacentrale@gmail.com'], // Enter here the email address on which you want to send emails from your customers
    from: senderName,
    subject: messageSubject,
    text: messageText,
    replyTo: senderEmail
  };

  if (senderName === '') {
    res.status(400);
    res.send({
      message: 'Bad request'
    });
    return;
  }

  if (senderEmail === '') {
    res.status(400);
    res.send({
      message: 'Bad request'
    });
    return;
  }

  if (messageSubject === '') {
    res.status(400);
    res.send({
      message: 'Bad request'
    });
    return;
  }

  if (messageText === '') {
    res.status(400);
    res.send({
      message: 'Bad request'
    });
    return;
  }

  if (copyToSender) {
    mailOptions.to.push(senderEmail);
  }

  transporter.sendMail(mailOptions, function (error, response) {
    if (error) {
      console.log(error);
      res.end('error');
    } else {
      console.log('Message sent: ', response);
      res.end('sent');
    }
  });
});

/*
//Fonction pour logout
userRouter.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/');
});

//Authentification via facebook
userRouter.get("/auth/facebook", passport.authenticate("facebook"));

userRouter.get("/auth/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/",
    failureRedirect: "/fail"
  }), function (req, res) {
    res.redirect('/');
  }
);
userRouter.get("/fail", (req, res) => {
  res.send("Failed attempt");
});
userRouter.get("/", (req, res) => {
  res.send("Success");
});

userRouter.get('/facebook-token', passport.authenticate('facebook-token', { session: false }),
  (req, res, next) => {
    res.send({
      token: jwt.sign(req.user, config.FACEBOOK_CLIENT_SECRET, {})
    })
  });



userRouter.post('/image_profile', function (req, res) {

  let userToUpdate;
  let urlRetour ="";
  let userId = ""; 
  let storage = multer.diskStorage({
    destination: function (req, file, cb) {
          cb(null, 'uploads/')
      },
      filename: function (req, file, cb) {
        urlRetour =  file.fieldname + '-' + Date.now() + '.jpg'; 
          cb(null,urlRetour)
      }
    });

  let upload = multer({ storage: storage }).single('file');
  let user_id = multer({ storage: storage }).single('user_id');
  user_id(req,res,function(err){
    userId = req.body.user_id;
  });
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      console.log("erreur uploading image");
    } else if (err) {
        console.log("erreur uploading image");
    }else{

      User.findOne({ _id: userId})
        .then(user => {
          console.log("user", user);
          userToUpdate = user;
          userToUpdate.imageprofile = urlRetour ;
          console.log("usertoUpdate",userToUpdate);
          User.updateOne({ _id: userId}, userToUpdate)
            .then(() => res.status(200).json({ message: 'Utilisateur modifié !' ,url : urlRetour
          }))
            .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(404).json({ error }));
        res.status(200).send({
          success: true,
          message: "email is required",
          url : urlRetour
        });
        }
    });
});


//Fonction pour upload des images
userRouter.post('/file_upload', function (req, res) {
  console.log(req);
  let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      console.log("destination");

      cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
      console.log("filename");
      cb(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
  });
  let upload = multer({ storage: storage }).single('file');
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      console.log("erreur uploading image");
    } else if (err) {
      console.log("erreur uploading image");
    } else {
      res.json({
        success: true,
        message: 'Image uploaded!'

      });
    }
  });
});

//Get All users
userRouter.get('/users', (req, res) => {
  User.find()
    .then(users => res.status(200).json(users))
    .catch(error => res.status(400).json({ error }));
});

//Get User by id
userRouter.get("/users/:id", (req, res) => {

  User.findOne({ _id: req.params.id })
    .then(user => res.status(200).json(user).send(user))
    .catch(error => res.status(404).json({ error }));
});

//Update User
userRouter.put('/users/:id', (req, res) => {
  let userToUpdate;
  User.findOne({ _id: req.params.id })
    .then(user => {
      userToUpdate = user;
      userToUpdate.online = false;

      User.updateOne({ _id: req.params.id }, userToUpdate)
        .then(() => res.status(200).json({ message: 'Utilisateur modifié !' }))
        .catch(error => res.status(400).json({ error }));

    })
    .catch(error => res.status(404).json({ error }));
});

//Get users containing name
userRouter.get('/users/name/:name', (req, res) => {
  console.log("Param name : ", req.params.name);
  User.find({ username: { $regex: req.params.name + '.*', $options: 'i' } })
    .then(users => res.status(200).json(users))
    .catch(error => res.status(400).json({ error }));
});

//Ajout un ami dans la liste des amis
userRouter.post("/users", (req, res) => {
  if (!req.body.emetteur) {
    return res.status(400).send({
      message: "emetteur is required"
    });
  } else if (!req.body.username) {
    return res.status(400).send({
      message: "username is required"
    });
  } else if (!req.body.email) {
    return res.status(400).send({
      message: "email is required"
    });
  }

  User.findOne({ _id: req.body.emetteur })
    .then(user => {

      let newFriend = new User({
        _id: req.body.idAmi,
        username: req.body.username,
        emetteur: req.body.email
      });

      user.friendsList.push(newFriend);
      user.save((err) => {
        if (err) {
          return res.send({
            success: false,
            message: 'erreur pour stocker User'
          });
        }
      });
      res.status(200).json(user);
    })
    .catch(error => res.status(404).json({ error }));




  console.log("Tout va bien : ", req.body);
  res.send("Nouveau ami ajouté");


});
*/

module.exports = userRouter;