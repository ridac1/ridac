//La vérification de TOKEN
function verifyToken(req, res, next) {
    //Récuperer le Header
    const _header = req.headers['authorization'];
  
    // Vérifier si _header = undefined
    if (typeof _header !== 'undefined') {
      // Split at the space ' '
      const space_token = _header.split(' ');
      // récuperer le token
      const _token = space_token[1];
      // retour le token
      req.token = _token;
      //middleware
      next();
    } else {
      // erreur serveur
      res.sendStatus(403);
    }
  }

module.exports = verifyToken ; 