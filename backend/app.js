
var express = require('express');
var bodyParser  = require( "body-parser");
var userRouter  = require( './routes/user.route');
var payementRouter  = require( './routes/payement.route');

var mongoose  = require( 'mongoose');
var config  = require( './utils/config');

const stripe = require("stripe")(config.STRIPE_SECRET_KEY);

var User  = require( './models/user.model');
var cors  = require( 'cors');
const app = express();

//allow OPTIONS on all resources

app.use(cors());
app.options('*', cors());






//APP USE

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//connexion BD
/*mongoose.connect(
  config.MONGODB_URI, { userNewUrlParser: true }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "error db:"));
db.once('open', function () {
  //CORS
  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });
  app.use("/post", postRoute);
  app.use("/message", messageRoute);
  app.use("/notification", notificationRoute);
  app.use("/commentaire", commentaireRouter)
  app.use("/", userRouter);

  //Demarage du serveur
  app.listen(config.PORT, () => {
    console.log(`http://localhost:${config.PORT}/`);
  });
})
*/

//Connexion Base de données distantes
mongoose.connect(config.MONGODB_REMOTE_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Connexion à MongoDB réussie !');
    //CORS
    app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
      next();
    });
    
 
    app.use("/", userRouter);
    app.use("/payement", payementRouter);

    //Demarage du serveur
    app.listen(config.PORT, () => {
      console.log(`http://localhost:${config.PORT}/`);
    });
  })
  .catch(() => console.log('Connexion à MongoDB échouée !'));
