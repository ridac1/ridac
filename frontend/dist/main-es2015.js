(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<br><br><br><br><br><br><br><br><br>\r\n<br><br><br><br><br><br><br><br><br>\r\n<section class=\"section section-lg pt-lg-0 mt--200\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center text-center mb-lg\">\r\n      <div class=\"col-lg-8\">\r\n        <h2 class=\"display-3\">Les poles</h2>\r\n        <p class=\"lead text-muted\">La constitution des poles se reparti comme suit : </p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-12\">\r\n\r\n        <div class=\"row row-grid\">\r\n\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-primary rounded-circle mb-4\">\r\n                  <img src=\"./assets/img/icons/common/law.ico\" width=\"30\">\r\n                </div>\r\n                <h6 class=\"text-primary text-uppercase\">le pôle juridique</h6>\r\n                <p class=\"description mt-3\">pour la gestion de toutes les questions relatives à la  juridiction de la structure (etablissement de la documentation juridique;  conseils et accompagnement entre autre) .</p>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-primary mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-success rounded-circle mb-4\">\r\n                  <i class=\"ni ni-istanbul\"></i>\r\n                </div>\r\n                <h6 class=\"text-success text-uppercase\">le protocole d’organisation</h6>\r\n                <p class=\"description mt-3\">pour toutes les questions logistiques relatives à nos actions.</p>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-success mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-warning rounded-circle mb-4\">\r\n                  <i class=\"fa fa-bullhorn\"></i>\r\n                </div>\r\n                <h6 class=\"text-warning text-uppercase\">le pôle communication;</h6>\r\n                <p class=\"description mt-3\">qui comme l’a précisé le président de séance est l’un des cerveaux du groupe; en charge de la création de l’identité de\r\n                  marque de l’organisation et de tout l’aspect communicationnel de la\r\n                  structure.</p>\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-warning mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-info rounded-circle mb-4\">\r\n                  <i class=\"fa fa-address-book\"></i>\r\n                </div>\r\n                <h6 class=\"text-info text-uppercase\">le pôle presse & contenu</h6>\r\n                <p class=\"description mt-3\">chargé de produire le contenu adapté à notre\r\n                  stratégie de communication pour nos différents supports de\r\n                  communication .</p>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-info mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-danger rounded-circle mb-4\">\r\n                  <i class=\"fa fa-desktop\" aria-hidden=\"true\"></i>\r\n                </div>\r\n                <h6 class=\"text-danger text-uppercase\">le pôle informatique</h6>\r\n                <p class=\"description mt-3\">en charge de la réalisation du site internet pour\r\n                  l’instant mais dont le champs d’action sera étendu au fil de l’évolution de\r\n                  la structure et de ses besoins en terme de solutions IT. .</p>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-danger mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-lg-4\">\r\n            <div class=\"card card-lift--hover shadow border-0\">\r\n              <div class=\"card-body py-5\">\r\n                <div class=\"icon icon-shape icon-shape-primary rounded-circle mb-4\">\r\n                  <i class=\"fa fa-tasks\"></i>\r\n                </div>\r\n                <h6 class=\"text-primary text-uppercase\">le pôle management</h6>\r\n                <p class=\"description mt-3\">qui lui est l’organe de pilotage des activités, le centre\r\n                  de prises de décision et de l’orientation stratégique de l’organisation.</p>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-primary mt-4\">En savoir plus</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</section>\r\n<br><br><br><br><br><br><br><br><br>\r\n<section class=\"section section-lg\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center text-center mb-lg\">\r\n      <div class=\"col-lg-8\">\r\n        <h2 class=\"display-3\">Equipe de developpement</h2>\r\n        <p class=\"lead text-muted\">Bla bla bla bla bla  Bla bla bla bla blaBla bla bla bla blaBla bla bla bla blaBla bla bla bla bla.</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 col-lg-3 mb-5 mb-lg-0\">\r\n        <div class=\"px-4\">\r\n          <img src=\"./assets/img/theme/team-1-800x800.jpg\"\r\n               class=\"rounded-circle img-center img-fluid shadow shadow-lg--hover\" style=\"width: 200px;\">\r\n          <div class=\"pt-4 text-center\">\r\n            <h5 class=\"title\">\r\n              <span class=\"d-block mb-1\">Mamadou Bane</span>\r\n              <small class=\"h6 text-muted\">Web Developer</small>\r\n            </h5>\r\n            <div class=\"mt-3\">\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-warning btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-twitter\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-warning btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-facebook\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-warning btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-dribbble\"></i>\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-3 mb-5 mb-lg-0\">\r\n        <div class=\"px-4\">\r\n          <img src=\"./assets/img/theme/team-2-800x800.jpg\"\r\n               class=\"rounded-circle img-center img-fluid shadow shadow-lg--hover\" style=\"width: 200px;\">\r\n          <div class=\"pt-4 text-center\">\r\n            <h5 class=\"title\">\r\n              <span class=\"d-block mb-1\">Abdoulaye Fankafolo</span>\r\n              <small class=\"h6 text-muted\">Web Developer</small>\r\n            </h5>\r\n            <div class=\"mt-3\">\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-primary btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-twitter\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-primary btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-facebook\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-primary btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-dribbble\"></i>\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6 col-lg-3 mb-5 mb-lg-0\">\r\n        <div class=\"px-4\">\r\n          <img src=\"./assets/img/theme/team-3-800x800.jpg\"\r\n               class=\"rounded-circle img-center img-fluid shadow shadow-lg--hover\" style=\"width: 200px;\">\r\n          <div class=\"pt-4 text-center\">\r\n            <h5 class=\"title\">\r\n              <span class=\"d-block mb-1\">Faysal Mahamadou Maiga</span>\r\n              <small class=\"h6 text-muted\">Full Stack Developer</small>\r\n            </h5>\r\n            <div class=\"mt-3\">\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-info btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-twitter\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-info btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-facebook\"></i>\r\n              </a>\r\n              <a href=\"javascript:void(0)\" class=\"btn btn-info btn-icon-only rounded-circle\">\r\n                <i class=\"fa fa-dribbble\"></i>\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/action/action.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/action/action.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>action works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/activities/activities.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/activities/activities.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<br><br><br><br><br><br><br><br><br>\r\n<section class=\"section section-lg pt-lg-0 mt--200\">\r\n  <br><br><br><br><br><br>\r\n  <div class=\"container\">\r\n    <h3 class=\"h4 text-success font-weight-bold mb-5\">Nos Activités</h3>\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-12\">\r\n\r\n        <ngb-tabset [justify]=\"'center'\" class=\"custom-tab-content flex-column flex-md-row\" type=\"pills\">\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <img src=\"./assets/img/icons/common/law.ico\" width=\"25\"> Juridique\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p>Rien pour l'instant</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <i class=\"ni ni-istanbul\"></i> Organisation\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p class=\"description\">Le protocole d’organisation a conformément à la première action en cours à\r\n                savoir une action de lutte contre le Covi-19 en Afrique; action phare sur laquelle\r\n                la construction de notre crédibilité sera basé au travers de la communication sur\r\n                les réseaux sociaux, soumis un document relatif à la nature des dons, à la\r\n                réception et l’acheminement des dons vers les pays cibles et à l’organisation\r\n                pratique sur le terrain pour la réception et la distribution. il a aussi été proposé\r\n                l’élaboration de fiches par pays à remplir par les donateurs d’une part, qui\r\n                préciseraient un certain nombre d’information à savoir des informations\r\n                d’identification, la nature du don, les détails et les quantités; et d’autre part, par\r\n                les bénéficiaires qui reconnaîtront avoir bien réceptionné les dons (nature et\r\n                quantité toujours précisé). Ce point a fait l’objectif de discussion; certains\r\n                estimant que ce soit malsain de recueillir des informations personnelles des\r\n                bénéficiaires notamment leurs informations d’identification; et d’autres justifiant\r\n                cette proposition par une volonté de transparence vis à vis des partenaires et\r\n                donateurs et par mesure de sécurité pour la structure au cas où elle ferait face à\r\n                certaines accusations. Il a finalement été retenu de ne retenir que des\r\n                informations chiffrées relatives aux dons émis et aussi la localisation des actions.</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <i class=\"fa fa-bullhorn\"></i> Communication\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p class=\"description\">A ce niveau, l’équipe de communication nous a fait un point relatif à la création\r\n                de l’identité visuelle de la structure. En effet, le logo, une mini charte graphique\r\n                et les déclinaisons du logo avaient déjà été proposés et validés.\r\n                Aussi, ils ont énuméré un certain nombre d’activité à mener et il s’agissait de la\r\n                stratégie de communication, de la réalisation de spots publicitaires et de vidéos\r\n                documentaires.\r\n                Pour ce qui était des actions imminentes, ils fait une promesse de livraison d’une\r\n                stratégie de communication digitale pour le vendredi 03/04/2020 .\r\n                Il a aussi été suggéré que la sélection des membres de pôle soit effectuée sur la\r\n                base de compétences; diversifier les compétences et éviter de surcharger le\r\n                groupe de personne pas techniquement très efficace.</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <i class=\"fa fa-address-book\"></i>Presse et contenu\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p class=\"description\"> Cet organe a présenté son plan d’action qui a fait l'objet de discussion. Les uns\r\n                proposant que l’on se concentre sur du contenu apaisant au vu de la quantité de\r\n                nouvelles alarmantes liées à l’épidémie déjà diffusées..</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n\r\n\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <i class=\"fa fa-desktop\" aria-hidden=\"true\"></i> Informatique\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p class=\"description\">Les informaticiens nous ont présenté un point d’avancement sur les travaux de\r\n                réalisation du site internet. Et à jour, les pages finalisées étaient la page d’accueil\r\n                et de connexion. Ils ont également à la demande des autres participants,\r\n                présenté les technologies utilisées. Il s’agit de Angular pour le FrontEnd et\r\n                NodeJs pour le BackEnd.\r\n                Il leur a été proposé de réfléchir à la mise en place d’une application mobile pour\r\n                le long terme et le président de séance a souhaité que soit prévu sur le site, une\r\n                page de présentation des membres de l’équipe.</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n          <ngb-tab>\r\n            <ng-template ngbTabTitle>\r\n              <i class=\"fa fa-tasks\"></i> Management\r\n            </ng-template>\r\n            <ng-template ngbTabContent>\r\n              <p class=\"description\">Rien pour le moment.</p>\r\n            </ng-template>\r\n          </ngb-tab>\r\n\r\n        </ngb-tabset>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</section>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-navbar style=\"background-color: #172b4d;\"></app-navbar>\r\n\r\n<router-outlet></router-outlet>\r\n<app-footer></app-footer>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<section class=\"cmspage mtb-40\">\n  <div class=\"container\">\n      <div class=\"row justify-content-center\">\n        <div class=\"col-md-8\">\n<!-- Default form contact -->\n<form class=\"text-center border border-light p-5\" [formGroup]=\"contactForm\" (ngSubmit)=\"onSubmit()\">\n\n  <p class=\"h4 mb-4\">Nous contacté</p>\n\n  <!-- Name -->\n  <input type=\"text\" formControlName=\"contactFormName\" id=\"defaultContactFormName\" mdbInput\n         class=\"form-control mb-4\" placeholder=\"Name\">\n\n  <!-- Email -->\n  <input type=\"email\" formControlName=\"contactFormEmail\" id=\"defaultContactFormEmail\" mdbInput\n         class=\"form-control mb-4\" placeholder=\"E-mail\">\n\n  <!-- Subject -->\n  <label>Sujet</label>\n  <select formControlName=\"contactFormSubjects\" class=\"browser-default custom-select mb-4\">\n    <option value=\"\" disabled>Choose option</option>\n    <option value=\"Faire un don\" selected>Faire Un Don</option>\n    <option value=\"Adhésion\">Adhésion</option>\n    <option value=\"Subjection\">Subjection</option>\n    <option value=\"Information\">Information</option>\n  </select>\n\n  <!-- Message -->\n  <div class=\"form-group\">\n    <textarea formControlName=\"contactFormMessage\" class=\"form-control rounded-0\" mdbInput id=\"exampleFormControlTextarea2\"\n              rows=\"3\" placeholder=\"Message\"></textarea>\n  </div>\n\n  <!-- Copy -->\n  <mdb-checkbox [default]=\"true\" class=\"mb-4\">Envoie moi une copie de ce message</mdb-checkbox>\n\n  <!-- Send button -->\n  <button mdbBtn color=\"info\" outline=\"true\" block=\"true\" class=\"z-depth-0 my-4 waves-effect\"\n          mdbWavesEffect type=\"submit\" [disabled]=\"disabledSubmitButton\">Envoyer</button>\n\n</form>\n<!-- Default form contact -->\n\n        </div>\n        </div>\n        </div>\n</section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/donate/donate.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/donate/donate.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"section section-shaped section-lg\">\r\n\r\n  <div class=\"container pt-lg-md\">\r\n    <div class=\"card-header bg-white pb-5\">\r\n\r\n      <div class=\"text-muted text-center mb-4\">\r\n        <div class=\"display-1\">Faire Un Don</div>\r\n      </div>\r\n\r\n\r\n    </div>\r\n    <div class=\"container\">\r\n      <form #checkout=\"ngForm\" (ngSubmit)=\"onSubmit(checkout)\" class=\"checkout\" ngNativeValidate>\r\n\r\n\r\n        <div class=\"form-group  mb-3 \" >\r\n          <select class=\"form-control\" id=\"type\" name=\"type\" ngModel required is-valid>\r\n            <option value=\"\"  disabled selected>Je suis *</option>\r\n            <option value=\"particulier\">Un Particulier</option>\r\n            <option value=\"entreprise\">Une Entreprise</option>\r\n            <option value=\"association\">Une Association</option>\r\n          </select>\r\n        </div>\r\n\r\n\r\n        <div class=\"form-group mb-3 flex-nowrap\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fa fa-male\"></i></span>\r\n            </div>\r\n            <input class=\"form-control form-control-lg\" placeholder=\"Nom *\" type=\"text\" name=\"nom\" (focus)=\"focus=true\"\r\n                   (blur)=\"focus=false\" ngModel required>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fa fa-male\"></i></span>\r\n            </div>\r\n            <input class=\"form-control form-control-lg\" placeholder=\"Prénom *\" type=\"text\" name=\"prenom\" (focus)=\"focus=true\"\r\n                   (blur)=\"focus=false\" ngModel required>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n\r\n          <select class=\"form-control\" id=\"sexe\" name=\"sexe\" ngModel required>\r\n            <option value=\"\"  disabled selected>Sexe *</option>\r\n            <option value=\"H\">Homme</option>\r\n            <option value=\"F\">Femme</option>\r\n          </select>\r\n        </div>\r\n\r\n        <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\r\n            </div>\r\n            <input class=\"form-control form-control-lg\" placeholder=\" Email *\" type=\"email\" name=\"email\" (focus)=\"focus=true\"\r\n                   (blur)=\"focus=false\" ngModel required>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>\r\n            </div>\r\n            <input class=\"form-control form-control-lg\" placeholder=\"Téléphone (Optionnel)\" type=\"text\" name=\"tel\" (focus)=\"focus=true\"\r\n                   (blur)=\"focus=false\" ngModel>\r\n          </div>\r\n        </div>\r\n\r\n\r\n        <div class=\"form-group\">\r\n          <select class=\"form-control\" id=\"country\" name=\"country\" ngModel required>\r\n            <option value=\"\"  disabled selected>Pays *</option>\r\n            <option name=\"country\" *ngFor=\"let obj of data\" [value]=\"obj.alpha2Code\">{{obj.name}}</option>\r\n          </select>\r\n        </div>\r\n\r\n        <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fa fa-money\"></i></span>\r\n            </div>\r\n            <input class=\"form-control form-control-lg\" placeholder=\" Montant du don *\" type=\"text\" name=\"mnt\" (focus)=\"focus=true\"\r\n                   (blur)=\"focus=false\" ngModel required>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"form-group\">\r\n          <select class=\"form-control\" id=\"devise\" name=\"devise\" ngModel required>\r\n            <option value=\"\"  disabled selected>Devise *</option>\r\n            <option value=\"eur\">Euro</option>\r\n            <option value=\"usd\">Dollar</option>\r\n            <option value=\"xof\">FCFA</option>\r\n          </select>\r\n        </div>\r\n\r\n\r\n        <div class=\"card mb-3\">\r\n          <div class=\"card-body no-footer\">\r\n            <div class=\"row pt-2 pb-2\">\r\n              <div class=\"col-12\">\r\n                <ul class=\"nav nav-pills nav-fill mb-3\">\r\n                  <div class=\"nav-item w-100\"> <i class=\"fa fa-credit-card\"></i> Mode de paiement </div>\r\n                  <li class=\"nav-item\"><a href=\"#VI\" class=\"nav-link\"><i class=\"fa fa-dollar\"></i> Virement </a></li>\r\n                  <li class=\"nav-item\"><a href=\"\" class=\"nav-link\"><i class=\"fa fa-money\"></i> RIB </a></li>\r\n                  <li class=\"nav-item\"><a href=\"http://www.leetchi.com/fr/c/lRnG6vAl\" target=\"_blank\" class=\"nav-link\"><i\r\n                    class=\"fa fa-dollar\"></i> Leetchi </a></li>\r\n                </ul>\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"form-group mb-3\">\r\n          <label for=\"card-info\">Card Info *</label>\r\n          <div id=\"card-info\" #cardInfo></div>\r\n          <div id=\"card-errors\" role=\"alert\" *ngIf=\"error\">{{ error }}</div>\r\n        </div>\r\n<br>\r\n\r\n        <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n          <div class=\"input-group input-group-alternative\">\r\n            <div class=\"input-group-prepend\">\r\n              <span class=\"input-group-text\"><i class=\"fa fa-info\"></i></span>\r\n            </div>\r\n            <textarea class=\"form-control form-control-lg\" placeholder=\"Un petit mot pour Friqya. (Optionnel)\" name=\"comment\"\r\n                      type=\"text\" (focus)=\"focus=true\" (blur)=\"focus=false\" ngModel></textarea>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"row my-4\">\r\n          <div class=\"col-12\">\r\n            <div class=\"custom-control custom-control-alternative custom-checkbox\">\r\n              <input class=\"custom-control-input\" id=\"customCheckRegister\" type=\"checkbox\">\r\n              <label class=\"custom-control-label\" for=\"customCheckRegister\">\r\n                        <span>J’accepte de recevoir par email des informations de la part de FRIQYA.\r\n                        </span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"text-center\">\r\n          <button type=\"submit\" class=\"btn btn-success my-4\">Envoyer le Don</button>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--\r\n<div class=\"fixed-sn\">\r\n  <div class=\"col-lg-12 ml-auto mr-auto\">\r\n    <mdb-carousel [interval]=\"myInterval\" [noWrap]=\"noWrapSlides\" [(activeSlide)]=\"activeSlideIndex\" (activeSlideChange)=\"activeSlideChange()\">\r\n      <mdb-slide>\r\n        <img src=\"../../assets/img/home/slid1.jfif\" width=\"100%\">\r\n        <div class=\"carousel-caption\">\r\n          <h4>Slide 1</h4>\r\n          <p>Blalalalalallalal</p>\r\n          <div class=\"btn-wrapper\">\r\n            <a  class=\"btn btn-info btn-icon mb-3 mb-sm-0\">\r\n              <span class=\"btn-inner--icon\"><i class=\"fas fa-hand-holding-usd\"></i></span>\r\n              <span class=\"btn-inner--text\">Faire Un Don</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </mdb-slide>\r\n      <mdb-slide>\r\n        <img src=\"../../assets/img/home/slid2.jfif\" width=\"100%\">\r\n        <div class=\"carousel-caption\">\r\n          <h4>Slide 2</h4>\r\n          <p>Blalalalalallalal</p>\r\n          <div class=\"btn-wrapper\">\r\n            <a  class=\"btn btn-info btn-icon mb-3 mb-sm-0\">\r\n              <span class=\"btn-inner--icon\"><i class=\"fas fa-hand-holding-usd\"></i></span>\r\n              <span class=\"btn-inner--text\">Faire Un Don</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </mdb-slide>\r\n      <mdb-slide>\r\n        <img src=\"../../assets/img/home/slid3.jfif\" width=\"100%\">\r\n        <div class=\"carousel-caption\">\r\n          <h4>Slide 3</h4>\r\n          <p>Blalalalalallalal</p>\r\n          <div class=\"btn-wrapper\">\r\n            <a  class=\"btn btn-info btn-icon mb-3 mb-sm-0\">\r\n              <span class=\"btn-inner--icon\"><i class=\"fas fa-hand-holding-usd\"></i></span>\r\n              <span class=\"btn-inner--text\">Faire Un Don</span>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </mdb-slide>\r\n    </mdb-carousel>\r\n  </div>\r\n</div>\r\n\r\n-->\r\n<section class=\"section section-shaped\">\r\n  <div class=\"shape shape-style-1 shape-default\">\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n    <span></span>\r\n  </div>\r\n  <div class=\"container py-md\">\r\n    <div class=\"row justify-content-between align-items-center\">\r\n      <div class=\"col-lg-7 mb-5 mb-lg-0\">\r\n        <h1 class=\"text-white font-weight-light\">Lutte contre le COVID-19</h1>\r\n        <p class=\"lead text-white mt-4\">Un groupe de jeunes issus de plusieurs  pays d’Afrique sous le nom de FRIQYA, une action qui certes à elle seule ne saurait arrêter cette menace mais qui nous l’espérons pourra contribuer à freiner la propagation du virus et pourquoi pas, briser les chaînes de contamination.Il s’agit d’offrir aux populations les plus fragiles de certains pays africains (Côte d’Ivoire, Mali, Burkina, Sénégal et Tchad pour le moment) des produits de prévention et de premières nécessités dans la lutte actuelle, leur permettant ainsi de se conformer au même titre que chaque citoyen de la terre, aux mesures d’hygiène arrêtées par L’OMS.Des équipes de bénévoles ont décidé de donner de leur temps tout en prenant les précautions qu’il faut afin d’assurer la distribution dans les mesures d'hygiène nécessaires. Ils n’attendent que vous pour soutenir cet effort. </p>\r\n        <a href=\"https://demos.creative-tim.com/argon-design-system-angular/documentation/alerts?ref=adsa-bootstrap-carousel\" class=\"btn btn-white mt-4\" id=\"bl\"> Faire Un Don</a>\r\n      </div>\r\n      <div class=\"col-lg-5 mb-lg-auto\">\r\n        <br><br><br><br>\r\n        <div class=\"rounded shadow-lg overflow-hidden transform-perspective-right\">\r\n        <ngb-carousel>\r\n            <ng-template ngbSlide>\r\n              <img class=\"img-fluid\" src=\"assets/img/theme/img-1-1200x1000.jpg\" alt=\"First slide\">\r\n            </ng-template>\r\n            <ng-template ngbSlide>\r\n              <img class=\"img-fluid\" src=\"assets/img/theme/img-2-1200x1000.jpg\" alt=\"Second slide\">\r\n            </ng-template>\r\n          <ng-template ngbSlide>\r\n            <img class=\"img-fluid\" src=\"assets/img/theme/img-2-1200x1000.jpg\" alt=\"Second slide\">\r\n          </ng-template>\r\n          </ngb-carousel>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</section>\r\n<div class=\"card bg-success text-white\">\r\n  <div class=\"card-body display-1 row justify-content-center\">Actualités</div>\r\n</div>\r\n<br>\r\n<div class=\"row row-grid\"  class=\"row justify-content-center\">\r\n\r\n  <div class=\"card\" style=\"width: 18rem;  margin-left: 10px\" >\r\n    <img class=\"card-img-top\" src=\"assets/img/images_friqya/dons/mali/1.jpeg\" alt=\"Card image cap\" height=\"200\" width=\"10px\">\r\n    <div class=\"card-body\">\r\n      <h5 class=\"card-title\">Don Mali</h5>\r\n      <p class=\"card-text\">Le don du Mali a ete reslisé le 10/10/10.</p>\r\n      <a [routerLink]=\"['/news']\" class=\"btn btn-success\">plus de details</a>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card\" style=\"width: 18rem; margin-left: 10px\">\r\n    <img class=\"card-img-top\" src=\"assets/img/images_friqya/dons/mali/2.jpeg\" alt=\"Card image cap\" height=\"200\" width=\"10px\">\r\n    <div class=\"card-body\">\r\n      <h5 class=\"card-title\">Don Senegal</h5>\r\n      <p class=\"card-text\">Le don du Senegal a ete reslisé le 10/10/10.</p>\r\n      <a [routerLink]=\"['/news']\" class=\"btn btn-success\">plus de details</a>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card\" style=\"width: 18rem;  margin-left: 10px\">\r\n    <img class=\"card-img-top\" src=\"assets/img/images_friqya/dons/mali/3.jpeg\" alt=\"Card image cap\" height=\"200\" width=\"10px\">\r\n    <div class=\"card-body\">\r\n      <h5 class=\"card-title\">Don Ivoire</h5>\r\n      <p class=\"card-text\">Le don du Cote d Ivoire a ete reslisé le 10/10/10.</p>\r\n      <a [routerLink]=\"['/news']\" class=\"btn btn-success\">plus de details</a>\r\n    </div>\r\n  </div>\r\n</div>\r\n<br><br><br>\r\n<h3 style=\"text-align: center\">FRIQYA EN CHIFFRE</h3>\r\n<div class=\"row justify-content-center\">\r\n\r\n  <div style=\"width: 18rem;\">\r\n    <div class=\"card card-stats mb-4 mb-lg-0\">\r\n      <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col\">\r\n            <h5 class=\"card-title text-uppercase text-muted mb-0\">Nombre total de dons</h5>\r\n            <span class=\"h2 font-weight-bold mb-0\">5</span>\r\n          </div>\r\n          <div class=\"col-auto\">\r\n            <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n              <i class=\"fas fa-chart-bar\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n          <span class=\"text-success mr-2\"><i class=\"fa fa-arrow-up\"></i> 3.48%</span>\r\n          <span class=\"text-nowrap\">Depuis le mois pass</span>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div style=\"width: 18rem;\">\r\n    <div class=\"card card-stats mb-4 mb-lg-0\">\r\n      <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col\">\r\n            <h5 class=\"card-title text-uppercase text-muted mb-0\">Nombre de pays</h5>\r\n            <span class=\"h2 font-weight-bold mb-0\">5</span>\r\n          </div>\r\n          <div class=\"col-auto\">\r\n            <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n              <i class=\"fas fa-chart-bar\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n          <span class=\"text-success mr-2\"><i class=\"fa fa-arrow-up\"></i> 3.48%</span>\r\n          <span class=\"text-nowrap\">Since last month</span>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n  <div style=\"width: 18rem;\">\r\n    <div class=\"card card-stats mb-4 mb-lg-0\">\r\n      <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col\">\r\n            <h5 class=\"card-title text-uppercase text-muted mb-0\">Nombre de donateur </h5>\r\n            <span class=\"h2 font-weight-bold mb-0\">250</span>\r\n          </div>\r\n          <div class=\"col-auto\">\r\n            <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n              <i class=\"fas fa-chart-bar\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n          <span class=\"text-success mr-2\"><i class=\"fa fa-arrow-up\"></i> 3.48%</span>\r\n          <span class=\"text-nowrap\">Since last month</span>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div style=\"width: 18rem;\">\r\n    <div class=\"card card-stats mb-4 mb-lg-0\">\r\n      <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col\">\r\n            <h5 class=\"card-title text-uppercase text-muted mb-0\">Nombre de beneficiare</h5>\r\n            <span class=\"h2 font-weight-bold mb-0\">35</span>\r\n          </div>\r\n          <div class=\"col-auto\">\r\n            <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n              <i class=\"fas fa-chart-bar\"></i>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n          <span class=\"text-success mr-2\"><i class=\"fa fa-arrow-up\"></i> 3.48%</span>\r\n          <span class=\"text-nowrap\">Since last month</span>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/impact/impact.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/impact/impact.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>impact works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/landing/landing.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/landing/landing.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<br><br><br><br><br><br>\r\n\r\n<h1 class=\"display-1\">Dons Mali</h1>\r\n<ul id=\"thumbnailsListMali\">\r\n  <li *ngFor=\"let image of visibleImagesMali\" >\r\n    <a [routerLink] =\"['/image', image.id]\">\r\n      <img src=\"{{image.url}}\" class=\"tn\" width=\"200\" height=\"160\">\r\n    </a>\r\n  </li>\r\n</ul>\r\n\r\n\r\n<h1  class=\"display-1\">Dons Senegal</h1>\r\n<ul id=\"thumbnailsListSenegal\">\r\n  <li *ngFor=\"let image of visibleImagesSenegal\" >\r\n    <a [routerLink] =\"['/image', image.id]\">\r\n      <img src=\"{{image.url}}\" class=\"tn\" width=\"200\" height=\"160\">\r\n    </a>\r\n  </li>\r\n</ul>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main>\r\n  <section class=\"section section-shaped section-lg\">\r\n    <div class=\"shape shape-style-1 bg-gradient-default\">\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n    </div>\r\n    <div class=\"container pt-lg-md\">\r\n      <div class=\"row justify-content-center\">\r\n        <div class=\"col-lg-5\">\r\n          <div class=\"card bg-secondary shadow border-0\">\r\n            <div class=\"card-header bg-white pb-5\">\r\n              <div class=\"text-muted text-center mb-3\">\r\n                <b>Se Connecter avec</b>\r\n              </div>\r\n              <div class=\"btn-wrapper text-center\">\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-neutral btn-icon\">\r\n                  <span class=\"btn-inner--icon\">\r\n                    <i class=\"fa fa-facebook\"></i>\r\n                  </span>\r\n                  <span class=\"btn-inner--text\">Facebook</span>\r\n                </a>\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-neutral btn-icon\">\r\n                  <span class=\"btn-inner--icon\">\r\n                    <img src=\"./assets/img/icons/common/google.svg\">\r\n                  </span>\r\n                  <span class=\"btn-inner--text\">Google</span>\r\n                </a>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body px-lg-5 py-lg-5\">\r\n\r\n              <form role=\"form\" #myForm=\"ngForm\" (ngSubmit)=\"onSubmit(myForm)\" ngNativeValidate>\r\n                <div class=\"form-group mb-3\" [ngClass]=\"{'focused':focus===true}\">\r\n                  <div class=\"input-group input-group-alternative\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Email\" type=\"email\" name=\"email\" (focus)=\"focus=true\" (blur)=\"focus=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus1===true}\">\r\n                  <div class=\"input-group input-group-alternative\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Mot de passe\" type=\"password\" name=\"password\" (focus)=\"focus1=true\" (blur)=\"focus1=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n                <div class=\"custom-control custom-control-alternative custom-checkbox\">\r\n                  <input class=\"custom-control-input\" id=\" customCheckLogin\" type=\"checkbox\">\r\n                  <label class=\"custom-control-label\" for=\" customCheckLogin\">\r\n                    <span>Remember me</span>\r\n                  </label>\r\n                </div>\r\n                <div class=\"text-center\">\r\n                  <button type=\"submit\" class=\"btn btn-primary my-4\">Connexion</button>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n          <div class=\"row mt-3\">\r\n            <div class=\"col-6\">\r\n              <a href=\"javascript:void(0)\" class=\"text-light\">\r\n                <small>Mot de passe oublié ?</small>\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 text-right\">\r\n              <a href=\"javascript:void(0)\" class=\"text-light\">\r\n                <small>S'inscrire</small>\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</main>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/modal.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modal/modal.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>modal works!</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/new/new.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/new/new.component.html ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--\r\n<section class=\"section section-lg section-nucleo-icons pb-250\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-8 text-center\">\r\n        <h2 class=\"display-3\">News Media</h2>\r\n        <p class=\"lead\">\r\n          The official package contains over 21.000 icons which are looking great in combination with Argon Design System. Make sure you check all of them and use those that you like the most.\r\n        </p>\r\n        <div class=\"btn-wrapper\">\r\n          <a href=\"https://demos.creative-tim.com/argon-design-system-angular/documentation/icons?ref=adsa-nucleo-section\" class=\"btn btn-primary\">View demo icons</a>\r\n          <a href=\"https://nucleoapp.com/?ref=1712\" target=\"_blank\" class=\"btn btn-default mt-3 mt-md-0\">View all icons</a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"blur--hover\">\r\n      <a href=\"https://demos.creative-tim.com/argon-design-system-angular/documentation/icons?ref=adsa-nucleo-section\">\r\n        <div class=\"icons-container blur-item mt-5\" data-toggle=\"on-screen\">\r\n\r\n          <i class=\"icon ni ni-diamond\"></i>\r\n\r\n          <i class=\"icon icon-sm ni ni-album-2\"></i>\r\n          <i class=\"icon icon-sm ni ni-app\"></i>\r\n          <i class=\"icon icon-sm ni ni-atom\"></i>\r\n\r\n          <i class=\"icon ni ni-bag-17\"></i>\r\n          <i class=\"icon ni ni-bell-55\"></i>\r\n          <i class=\"icon ni ni-credit-card\"></i>\r\n\r\n          <i class=\"icon icon-sm ni ni-briefcase-24\"></i>\r\n          <i class=\"icon icon-sm ni ni-building\"></i>\r\n          <i class=\"icon icon-sm ni ni-button-play\"></i>\r\n\r\n          <i class=\"icon ni ni-calendar-grid-58\"></i>\r\n          <i class=\"icon ni ni-camera-compact\"></i>\r\n          <i class=\"icon ni ni-chart-bar-32\"></i>\r\n        </div>\r\n        <span class=\"blur-hidden h5 text-success\">Eplore all the 21.000+ Nucleo Icons</span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n-->\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main class=\"profile-page\">\n  <section class=\"section-profile-cover section-shaped my-0\">\n    <!-- Circles background -->\n    <div class=\"shape shape-style-1 shape-primary alpha-4\">\n      <span></span>\n      <span></span>\n      <span></span>\n      <span></span>\n      <span></span>\n      <span></span>\n      <span></span>\n    </div>\n    <!-- SVG separator -->\n    <div class=\"separator separator-bottom separator-skew\">\n      <svg x=\"0\" y=\"0\" viewBox=\"0 0 2560 100\" preserveAspectRatio=\"none\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n        <polygon class=\"fill-white\" points=\"2560 0 2560 100 0 100\"></polygon>\n      </svg>\n    </div>\n  </section>\n  <section class=\"section\">\n    <div class=\"container\">\n      <div class=\"card card-profile shadow mt--300\">\n        <div class=\"px-4\">\n          <div class=\"row justify-content-center\">\n            <div class=\"col-lg-3 order-lg-2\">\n              <div class=\"card-profile-image\">\n                <a href=\"javascript:void(0)\">\n                  <img src=\"../../assets/img/avatar.png\" class=\"rounded-circle\">\n                </a>\n              </div>\n            </div>\n            <div class=\"col-lg-4 order-lg-3 text-lg-right align-self-lg-center\">\n              <div class=\"card-profile-actions py-4 mt-lg-0\">\n                <a href=\"javascript:void(0)\" class=\"btn btn-sm btn-info mr-4\">Connect</a>\n                <a href=\"javascript:void(0)\" class=\"btn btn-sm btn-default float-right\">Message</a>\n              </div>\n            </div>\n            <div class=\"col-lg-4 order-lg-1\">\n              <div class=\"card-profile-stats d-flex justify-content-center\">\n                <div>\n                  <span class=\"heading\">22</span>\n                  <span class=\"description\">Friends</span>\n                </div>\n                <div>\n                  <span class=\"heading\">10</span>\n                  <span class=\"description\">Photos</span>\n                </div>\n                <div>\n                  <span class=\"heading\">89</span>\n                  <span class=\"description\">Comments</span>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"text-center mt-5\">\n            <h3>{{user.firstname}} {{user.lastname}}\n              <span class=\"font-weight-light\"> </span>\n            </h3>\n            <div class=\"h6 font-weight-300\"><i class=\"ni location_pin mr-2\"></i>{{user.email}}</div>\n            <div class=\"h6 mt-4\"><i class=\"ni business_briefcase-24 mr-2\"></i>{{user.tel}}</div>\n            <div><i class=\"ni education_hat mr-2\"></i>{{user.address}}</div>\n          </div>\n          <div class=\"mt-5 py-5 border-top text-center\">\n            <div class=\"row justify-content-center\">\n              <div class=\"col-lg-9\">\n                <p>An artist of considerable range, Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music, giving it a warm, intimate feel with a solid groove structure. An artist of considerable range.</p>\n                <a href=\"javascript:void(0)\">Show more</a>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</main>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/footer/footer.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/footer/footer.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"footer\" [ngClass]=\"{'has-cards': getPath()!=='/user-profile' && getPath()!=='/register' && getPath()!=='/login'}\">\r\n\r\n  <div class=\"container\">\r\n    <div class=\"row row-grid align-items-center\" [ngClass]=\"{'my-md': getPath()!=='/user-profile' && getPath()!=='/register' && getPath()!=='/login', 'mb-5':getPath()==='/user-profile' || getPath()==='/register' || getPath()==='/login'}\">\r\n      <div class=\"col-lg-6\">\r\n        <h3 class=\"text-primary font-weight-light mb-2\">Merci d avoir apporter votre aide!</h3>\r\n        <h4 class=\"mb-0 font-weight-light\">Venons en aide quand on peut.</h4>\r\n      </div>\r\n      <div class=\"col-lg-6 text-lg-center btn-wrapper\">\r\n        <a target=\"_blank\" href=\"https://twitter.com/creativetim\" class=\"btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg\" ngbTooltip=\"Follow us\">\r\n          <i class=\"fa fa-twitter\"></i>\r\n        </a>\r\n        <a target=\"_blank\" href=\"https://www.facebook.com/creativetim\" class=\"btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg\" ngbTooltip=\"Like us\">\r\n          <i class=\"fa fa-facebook-square\"></i>\r\n        </a>\r\n      </div>\r\n    </div>\r\n    <hr>\r\n    <div class=\"row align-items-center justify-content-md-between\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"copyright\">\r\n          &copy; {{test | date: 'yyyy'}}\r\n          <a href=\"https://www.creative-tim.com?ref=adsa-footer\" target=\"_blank\">Equipe FRIQYA</a>.\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6\">\r\n        <ul class=\"nav nav-footer justify-content-end\">\r\n          <li class=\"nav-item\">\r\n            <a href=\"https://www.creative-tim.com?ref=adsa-footer\" class=\"nav-link\" target=\"_blank\">Equipe FRIQYA</a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a href=\"https://www.creative-tim.com/presentation?ref=adsa-footer\" class=\"nav-link\" target=\"_blank\">A propos de nous</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</footer>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/navbar/navbar.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/navbar/navbar.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav id=\"navbar-main\" class=\"navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom headroom--top headroom--pinned\">\r\n  <div class=\"container\">\r\n    <a class=\"navbar-brand mr-lg-5\" [routerLink]=\"['/home']\">\r\n      <img src=\"../../../assets/img/logo/logo.png\">\r\n    </a>\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed = !isCollapsed\"\r\n          [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"navbar_global\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div class=\"navbar-collapse collapse\" id=\"navbar_global\" [ngbCollapse]=\"isCollapsed\">\r\n      <div class=\"navbar-collapse-header\">\r\n        <div class=\"row\">\r\n          <div class=\"col-6 collapse-brand\">\r\n            <a [routerLink]=\"['/home']\">\r\n              <img src=\"./assets/img/brand/blue.png\">\r\n            </a>\r\n          </div>\r\n          <div class=\"col-6 collapse-close\">\r\n            <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed = !isCollapsed\"\r\n                  [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"navbar_global\">\r\n              <span></span>\r\n              <span></span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <ul class=\"navbar-nav navbar-nav-hover align-items-lg-center\">\r\n\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/home']\">\r\n            <span class=\"nav-link-inner--text\">Accueil</span>\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/about']\">\r\n            <span class=\"nav-link-inner--text\">A Propos</span>\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/activity']\">\r\n            <span class=\"nav-link-inner--text\">Activités</span>\r\n          </a>\r\n        </li>\r\n        <!--\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link no-caret\" data-toggle=\"dropdown\" role=\"button\">\r\n            <i class=\"ni ni-collection d-lg-none\"></i>\r\n            <span class=\"nav-link-inner--text\">Propos</span>\r\n          </a>\r\n          <div class=\"dropdown-menu\">\r\n            <a [routerLink]=\"['/landing']\" class=\"dropdown-item\">Equipe</a>\r\n            <a [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">Missions</a>\r\n            <a [routerLink]=\"['/login']\" class=\"dropdown-item\">Etre Benevol</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link no-caret\" data-toggle=\"dropdown\" role=\"button\">\r\n            <i class=\"ni ni-collection d-lg-none\"></i>\r\n            <span class=\"nav-link-inner--text\">Actions</span>\r\n          </a>\r\n          <div class=\"dropdown-menu\">\r\n            <a [routerLink]=\"['/landing']\" class=\"dropdown-item\">Explication Général</a>\r\n            <a [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">Rapports Hebdomadaires</a>\r\n            <a [routerLink]=\"['/login']\" class=\"dropdown-item\">Temoignages</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link no-caret\" data-toggle=\"dropdown\" role=\"button\">\r\n            <i class=\"ni ni-collection d-lg-none\"></i>\r\n            <span class=\"nav-link-inner--text\">Impacts</span>\r\n          </a>\r\n          <div class=\"dropdown-menu\">\r\n            <a [routerLink]=\"['/landing']\" class=\"dropdown-item\">Vidéos</a>\r\n            <a [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">Photos & Graphiques</a>\r\n          </div>\r\n        </li>-->\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/landing']\">\r\n            <span class=\"nav-link-inner--text\">News</span>\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/contact']\">\r\n            <span class=\"nav-link-inner--text\">Contact</span>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n      <ul class=\"navbar-nav align-items-lg-center ml-lg-auto\">\r\n\r\n        <li class=\"nav-item d-none d-lg-block ml-lg-4\">\r\n          <a  [routerLink]=\"['/donate']\" class=\"btn btn-neutral btn-icon\" id=\"bl\">\r\n            <span class=\"btn-inner--icon\">\r\n              <!--<i class=\"fa fa-cloud-download mr-2\"></i>-->\r\n            </span>\r\n            <span class=\"nav-link-inner--text\">Faire Un Don</span>\r\n          </a>\r\n        </li>\r\n        <li *ngIf=\"!isLogged\" class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/register']\">\r\n            <span class=\"nav-link-inner--text\">Inscription</span>\r\n          </a>\r\n        </li>\r\n        <li *ngIf=\"!isLogged\" class=\"nav-item\">\r\n          <a class=\"nav-link\" [routerLink]=\"['/login']\">\r\n            <span class=\"nav-link-inner--text\">Connexion</span>\r\n          </a>\r\n        </li>\r\n        <li *ngIf=\"isLogged\" class=\"nav-item\">\r\n          <a class=\"nav-link\" (click)=\"logout()\">\r\n            <span class=\"nav-link-inner--text\">Deconnecté</span>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main>\r\n  <section class=\"section section-shaped section-lg\">\r\n    <div class=\"shape shape-style-1 bg-gradient-default\">\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n    </div>\r\n    <div class=\"container pt-lg-md\">\r\n      <div class=\"row justify-content-center\">\r\n        <div class=\"col-lg-5\">\r\n          <div class=\"card bg-secondary shadow border-0\">\r\n            <div class=\"card-header bg-white pb-5\">\r\n              <div class=\"text-muted text-center mb-3\">\r\n                <b>S'inscrire avec </b>\r\n              </div>\r\n              <div class=\"text-center\">\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-neutral btn-icon mr-4\">\r\n                  <span class=\"btn-inner--icon\">\r\n                    <img src=\"./assets/img/icons/common/facebook.png\">\r\n                  </span>\r\n                  <span class=\"btn-inner--text\">Facebook</span>\r\n                </a>\r\n\r\n                <a href=\"javascript:void(0)\" class=\"btn btn-neutral btn-icon\">\r\n                  <span class=\"btn-inner--icon\">\r\n                    <img src=\"./assets/img/icons/common/google.svg\">\r\n                  </span>\r\n                  <span class=\"btn-inner--text\">Google</span>\r\n                </a>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-body px-lg-5 py-lg-5\">\r\n\r\n              <form role=\"form\" #myForm=\"ngForm\" (ngSubmit)=\"onSubmit(myForm)\" ngNativeValidate>\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus===true}\">\r\n                  <div class=\"input-group input-group-alternative mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"fa fa-user\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Nom \" type=\"text\" name=\"nom\" (focus)=\"focus=true\" (blur)=\"focus=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus===true}\">\r\n                  <div class=\"input-group input-group-alternative mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"fa fa-user\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Prenom \" type=\"text\" name=\"prenom\" (focus)=\"focus=true\" (blur)=\"focus=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n                <div>\r\n                  <label>Sexe</label> <br>\r\n                  <select  style=\"width:300px; height: 30px;\"  name=\"sexe\" ngModel required>\r\n                    <option  value=\"homme\">Homme</option>\r\n                    <option value=\"femme\">Femme</option>\r\n                  </select>\r\n                </div>\r\n                <br>\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus1===true}\">\r\n                  <div class=\"input-group input-group-alternative mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Email\" type=\"email\" name=\"email\" (focus)=\"focus1=true\" (blur)=\"focus1=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus===true}\">\r\n                  <div class=\"input-group input-group-alternative mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"fa fa-phone\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Telephone\" type=\"text\"  name=\"tel\"(focus)=\"focus=true\" (blur)=\"focus=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus===true}\">\r\n                  <div class=\"input-group input-group-alternative mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"fa fa-address-card\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Adresse\" type=\"text\" name=\"adresse\" (focus)=\"focus=true\" (blur)=\"focus=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus2===true}\">\r\n                  <div class=\"input-group input-group-alternative\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Mot de passe\" type=\"password\" name=\"password\" (focus)=\"focus2=true\" (blur)=\"focus2=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\" [ngClass]=\"{'focused':focus2===true}\">\r\n                  <div class=\"input-group input-group-alternative\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>\r\n                    </div>\r\n                    <input class=\"form-control\" placeholder=\"Repeter mot de passe\" type=\"password\" name=\"password2\" (focus)=\"focus2=true\" (blur)=\"focus2=false\" ngModel required>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"row my-4\">\r\n                  <div class=\"col-12\">\r\n                    <div class=\"custom-control custom-control-alternative custom-checkbox\">\r\n                      <input class=\"custom-control-input\" id=\"customCheckRegister\" type=\"checkbox\">\r\n                      <label class=\"custom-control-label\" for=\"customCheckRegister\">\r\n                        <span>Je suis d'accord avec\r\n                          <a href=\"javascript:void(0)\">Politique de confidentialité</a>\r\n                        </span>\r\n                      </label>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"text-center\">\r\n                  <button type=\"submit\" class=\"btn btn-primary mt-4\">Creer Compte</button>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</main>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/strip-payement/strip-payement.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/strip-payement/strip-payement.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<br>\n<br>\n<br>\n<br>\n<br>\n<form #checkout=\"ngForm\" (ngSubmit)=\"onSubmit(checkout)\" class=\"checkout\">\n  <div class=\"form-row\">\n    <label for=\"card-info\">Card Info</label>\n    <div id=\"card-info\" #cardInfo style=\"width: 8000px\"></div>\n    <div id=\"card-errors\" role=\"alert\" *ngIf=\"error\">{{ error }}</div>\n  </div>\n\n  <button type=\"submit\">Pay $777</button>\n</form>\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.css":
/*!*******************************************!*\
  !*** ./src/app/about/about.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AboutComponent = class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
};
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], AboutComponent.prototype, "isLogged", void 0);
AboutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./about.component.css */ "./src/app/about/about.component.css")).default]
    })
], AboutComponent);



/***/ }),

/***/ "./src/app/action/action.component.css":
/*!*********************************************!*\
  !*** ./src/app/action/action.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGlvbi9hY3Rpb24uY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/action/action.component.ts":
/*!********************************************!*\
  !*** ./src/app/action/action.component.ts ***!
  \********************************************/
/*! exports provided: ActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionComponent", function() { return ActionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ActionComponent = class ActionComponent {
    constructor() { }
    ngOnInit() {
    }
};
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ActionComponent.prototype, "isLogged", void 0);
ActionComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-action',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./action.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/action/action.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./action.component.css */ "./src/app/action/action.component.css")).default]
    })
], ActionComponent);



/***/ }),

/***/ "./src/app/activities/activities.component.css":
/*!*****************************************************!*\
  !*** ./src/app/activities/activities.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGl2aXRpZXMvYWN0aXZpdGllcy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/activities/activities.component.ts":
/*!****************************************************!*\
  !*** ./src/app/activities/activities.component.ts ***!
  \****************************************************/
/*! exports provided: ActivitiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivitiesComponent", function() { return ActivitiesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ActivitiesComponent = class ActivitiesComponent {
    constructor() { }
    ngOnInit() {
    }
};
ActivitiesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-activities',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./activities.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/activities/activities.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./activities.component.css */ "./src/app/activities/activities.component.css")).default]
    })
], ActivitiesComponent);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm2015/add/operator/filter.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");






var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = 0;
let AppComponent = class AppComponent {
    constructor(renderer, router, document, element, location) {
        this.renderer = renderer;
        this.router = router;
        this.document = document;
        this.element = element;
        this.location = location;
        this.isLoggedIn = false;
    }
    hasScrolled() {
        var st = window.pageYOffset;
        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        var navbar = document.getElementsByTagName('nav')[0];
        // If they scrolled down and are past the navbar, add class .headroom--unpinned.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            if (navbar.classList.contains('headroom--pinned')) {
                navbar.classList.remove('headroom--pinned');
                navbar.classList.add('headroom--unpinned');
            }
            // $('.navbar.headroom--pinned').removeClass('headroom--pinned').addClass('headroom--unpinned');
        }
        else {
            // Scroll Up
            //  $(window).height()
            if (st + window.innerHeight < document.body.scrollHeight) {
                // $('.navbar.headroom--unpinned').removeClass('headroom--unpinned').addClass('headroom--pinned');
                if (navbar.classList.contains('headroom--unpinned')) {
                    navbar.classList.remove('headroom--unpinned');
                    navbar.classList.add('headroom--pinned');
                }
            }
        }
        lastScrollTop = st;
    }
    ;
    ngOnInit() {
        var navbar = this.element.nativeElement.children[0].children[0];
        this._router = this.router.events.filter(event => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]).subscribe((event) => {
            if (window.outerWidth > 991) {
                window.document.children[0].scrollTop = 0;
            }
            else {
                window.document.activeElement.scrollTop = 0;
            }
            this.renderer.listen('window', 'scroll', (event) => {
                const number = window.scrollY;
                if (number > 150 || window.pageYOffset > 150) {
                    // add logic
                    navbar.classList.add('headroom--not-top');
                }
                else {
                    // remove logic
                    navbar.classList.remove('headroom--not-top');
                }
            });
        });
        this.hasScrolled();
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["DOCUMENT"],] }] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] }
];
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', ['$event'])
], AppComponent.prototype, "hasScrolled", null);
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_4__["DOCUMENT"]))
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm2015/angular-bootstrap-md.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _landing_landing_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./landing/landing.component */ "./src/app/landing/landing.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
/* harmony import */ var _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared/footer/footer.component */ "./src/app/shared/footer/footer.component.ts");
/* harmony import */ var _home_home_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./home/home.module */ "./src/app/home/home.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _action_action_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./action/action.component */ "./src/app/action/action.component.ts");
/* harmony import */ var _new_new_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./new/new.component */ "./src/app/new/new.component.ts");
/* harmony import */ var _impact_impact_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./impact/impact.component */ "./src/app/impact/impact.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _donate_donate_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./donate/donate.component */ "./src/app/donate/donate.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _strip_payement_strip_payement_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./strip-payement/strip-payement.component */ "./src/app/strip-payement/strip-payement.component.ts");
/* harmony import */ var ng_stripe_checkout__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ng-stripe-checkout */ "./node_modules/ng-stripe-checkout/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _activities_activities_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./activities/activities.component */ "./src/app/activities/activities.component.ts");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./modal/modal.component */ "./src/app/modal/modal.component.ts");
/* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./services/image.service */ "./src/app/services/image.service.ts");





























let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
            _signup_signup_component__WEBPACK_IMPORTED_MODULE_9__["SignupComponent"],
            _landing_landing_component__WEBPACK_IMPORTED_MODULE_10__["LandingComponent"],
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"],
            _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__["NavbarComponent"],
            _shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_13__["FooterComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_16__["AboutComponent"],
            _action_action_component__WEBPACK_IMPORTED_MODULE_17__["ActionComponent"],
            _new_new_component__WEBPACK_IMPORTED_MODULE_18__["NewComponent"],
            _impact_impact_component__WEBPACK_IMPORTED_MODULE_19__["ImpactComponent"],
            _contact_contact_component__WEBPACK_IMPORTED_MODULE_20__["ContactComponent"],
            _donate_donate_component__WEBPACK_IMPORTED_MODULE_21__["DonateComponent"],
            _strip_payement_strip_payement_component__WEBPACK_IMPORTED_MODULE_23__["StripPayementComponent"],
            _activities_activities_component__WEBPACK_IMPORTED_MODULE_26__["ActivitiesComponent"],
            _modal_modal_component__WEBPACK_IMPORTED_MODULE_27__["ModalComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
            _app_routing__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _home_home_module__WEBPACK_IMPORTED_MODULE_14__["HomeModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
            ng_stripe_checkout__WEBPACK_IMPORTED_MODULE_24__["StripeCheckoutModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_25__["BrowserAnimationsModule"],
            angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_7__["MDBBootstrapModule"].forRoot()
        ],
        providers: [_services_image_service__WEBPACK_IMPORTED_MODULE_28__["ImageService"]],
        exports: [
            _new_new_component__WEBPACK_IMPORTED_MODULE_18__["NewComponent"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _landing_landing_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./landing/landing.component */ "./src/app/landing/landing.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _donate_donate_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./donate/donate.component */ "./src/app/donate/donate.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _activities_activities_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./activities/activities.component */ "./src/app/activities/activities.component.ts");
/* harmony import */ var _strip_payement_strip_payement_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./strip-payement/strip-payement.component */ "./src/app/strip-payement/strip-payement.component.ts");















const routes = [
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"] },
    { path: 'user-profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__["ProfileComponent"] },
    { path: 'register', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_7__["SignupComponent"] },
    { path: 'landing', component: _landing_landing_component__WEBPACK_IMPORTED_MODULE_8__["LandingComponent"] },
    { path: 'donate', component: _donate_donate_component__WEBPACK_IMPORTED_MODULE_10__["DonateComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_12__["AboutComponent"] },
    { path: 'contact', component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_11__["ContactComponent"] },
    { path: 'activity', component: _activities_activities_component__WEBPACK_IMPORTED_MODULE_13__["ActivitiesComponent"] },
    { path: 'pay', component: _strip_payement_strip_payement_component__WEBPACK_IMPORTED_MODULE_14__["StripPayementComponent"] },
    { path: 'news', component: _landing_landing_component__WEBPACK_IMPORTED_MODULE_8__["LandingComponent"] },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(routes, {
                useHash: true
            })
        ],
        exports: [],
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/contact/contact.component.css":
/*!***********************************************!*\
  !*** ./src/app/contact/contact.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_contact_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/contact.service */ "./src/app/services/contact.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");




let ContactComponent = class ContactComponent {
    constructor(fb, connectionService) {
        this.fb = fb;
        this.connectionService = connectionService;
        this.disabledSubmitButton = true;
        this.contactForm = fb.group({
            'contactFormName': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            'contactFormEmail': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
            'contactFormSubjects': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            'contactFormMessage': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            'contactFormCopy': [''],
        });
    }
    oninput() {
        if (this.contactForm.valid) {
            this.disabledSubmitButton = false;
        }
    }
    onSubmit() {
        this.connectionService.sendMessage(this.contactForm.value).subscribe(() => {
            alert('Your message has been sent.');
            this.contactForm.reset();
            this.disabledSubmitButton = true;
        }, error => {
            console.log('Error', error);
        });
    }
};
ContactComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_contact_service__WEBPACK_IMPORTED_MODULE_1__["ContactService"] }
];
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["HostListener"])('input')
], ContactComponent.prototype, "oninput", null);
ContactComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact.component.css */ "./src/app/contact/contact.component.css")).default]
    })
], ContactComponent);



/***/ }),

/***/ "./src/app/donate/donate.component.css":
/*!*********************************************!*\
  !*** ./src/app/donate/donate.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RvbmF0ZS9kb25hdGUuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/donate/donate.component.ts":
/*!********************************************!*\
  !*** ./src/app/donate/donate.component.ts ***!
  \********************************************/
/*! exports provided: DonateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DonateComponent", function() { return DonateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_liste_pays_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/liste-pays.service */ "./src/app/services/liste-pays.service.ts");





let DonateComponent = class DonateComponent {
    constructor(authService, route, listePaysService, cd) {
        this.authService = authService;
        this.route = route;
        this.listePaysService = listePaysService;
        this.cd = cd;
        this.date = new Date();
        this.form = {};
        this.payInfo = {
            type: null,
            nom: null,
            prenom: null,
            email: null,
            pays: null,
            tel: null,
            sexe: null,
            montant: null,
            devise: null,
            cardTocken: null,
            mot: null
        };
        this.cardHandler = this.onChange.bind(this);
    }
    ngOnInit() {
        this.getData();
    }
    getData() {
        this.listePaysService.getData().subscribe(data => {
            this.data = data;
            console.log(this.data);
        });
    }
    ngAfterViewInit() {
        const style = {
            base: {
                lineHeight: '24px',
                fontFamily: 'monospace',
                fontSmoothing: 'antialiased',
                fontSize: '19px',
                '::placeholder': {
                    color: 'purple'
                }
            }
        };
        console.log(this.cardInfo);
        this.card = elements.create('card');
        this.card.mount(this.cardInfo.nativeElement);
        this.card.addEventListener('change', this.cardHandler);
    }
    ngOnDestroy() {
        this.card.removeEventListener('change', this.cardHandler);
        this.card.destroy();
    }
    onChange({ error }) {
        if (error) {
            this.error = error.message;
        }
        else {
            this.error = null;
        }
        this.cd.detectChanges();
    }
    onSubmit(form) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const { token, error } = yield stripe.createToken(this.card);
            this.payInfo.nom = form.value.nom;
            this.payInfo.prenom = form.value.prenom;
            this.payInfo.type = form.value.type;
            this.payInfo.email = form.value.email;
            this.payInfo.tel = form.value.tel;
            this.payInfo.sexe = form.value.sexe;
            this.payInfo.pays = form.value.country;
            this.payInfo.montant = form.value.mnt;
            this.payInfo.devise = form.value.devise;
            this.payInfo.mot = form.value.comment;
            if (error) {
                console.log('Something is wrong:', error);
            }
            else {
                console.log('Success!', token);
                // ...send the token to the your backend to process the charge
                this.payInfo.cardTocken = token;
                this.authService.pay(this.payInfo).subscribe(data => {
                    console.log(data);
                }, error => {
                    console.log(error);
                });
            }
        });
    }
};
DonateComponent.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_liste_pays_service__WEBPACK_IMPORTED_MODULE_4__["ListePaysService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
];
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('cardInfo')
], DonateComponent.prototype, "cardInfo", void 0);
DonateComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-donate',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./donate.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/donate/donate.component.html")).default,
        providers: [_services_liste_pays_service__WEBPACK_IMPORTED_MODULE_4__["ListePaysService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./donate.component.css */ "./src/app/donate/donate.component.css")).default]
    })
], DonateComponent);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#bl {\n  -webkit-animation: blinker 3s linear infinite;\n          animation: blinker 3s linear infinite;\n  color: #1c87c9;\n}\n\n@-webkit-keyframes blinker {\n  50% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker {\n  50% {\n    opacity: 0;\n  }\n}\n\n.blink-one {\n  -webkit-animation: blinker-one 1s linear infinite;\n          animation: blinker-one 1s linear infinite;\n}\n\n@-webkit-keyframes blinker-one {\n  0% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker-one {\n  0% {\n    opacity: 0;\n  }\n}\n\n.blink-two {\n  -webkit-animation: blinker-two 1.4s linear infinite;\n          animation: blinker-two 1.4s linear infinite;\n}\n\n@-webkit-keyframes blinker-two {\n  100% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker-two {\n  100% {\n    opacity: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcUmVwb3NcXHJpZGFjXFxmcm9udGVuZC9zcmNcXGFwcFxcaG9tZVxcaG9tZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNkNBQUE7VUFBQSxxQ0FBQTtFQUNBLGNBQUE7QUNDRjs7QURDQTtFQUNFO0lBQU0sVUFBQTtFQ0dOO0FBQ0Y7O0FETEE7RUFDRTtJQUFNLFVBQUE7RUNHTjtBQUNGOztBREZBO0VBQ0UsaURBQUE7VUFBQSx5Q0FBQTtBQ0lGOztBREZBO0VBQ0U7SUFBSyxVQUFBO0VDTUw7QUFDRjs7QURSQTtFQUNFO0lBQUssVUFBQTtFQ01MO0FBQ0Y7O0FETEE7RUFDRSxtREFBQTtVQUFBLDJDQUFBO0FDT0Y7O0FETEE7RUFDRTtJQUFPLFVBQUE7RUNTUDtBQUNGOztBRFhBO0VBQ0U7SUFBTyxVQUFBO0VDU1A7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNibCB7XHJcbiAgYW5pbWF0aW9uOiBibGlua2VyIDNzIGxpbmVhciBpbmZpbml0ZTtcclxuICBjb2xvcjogIzFjODdjOTtcclxufVxyXG5Aa2V5ZnJhbWVzIGJsaW5rZXIge1xyXG4gIDUwJSB7IG9wYWNpdHk6IDA7IH1cclxufVxyXG4uYmxpbmstb25lIHtcclxuICBhbmltYXRpb246IGJsaW5rZXItb25lIDFzIGxpbmVhciBpbmZpbml0ZTtcclxufVxyXG5Aa2V5ZnJhbWVzIGJsaW5rZXItb25lIHtcclxuICAwJSB7IG9wYWNpdHk6IDA7IH1cclxufVxyXG4uYmxpbmstdHdvIHtcclxuICBhbmltYXRpb246IGJsaW5rZXItdHdvIDEuNHMgbGluZWFyIGluZmluaXRlO1xyXG59XHJcbkBrZXlmcmFtZXMgYmxpbmtlci10d28ge1xyXG4gIDEwMCUgeyBvcGFjaXR5OiAwOyB9XHJcbn1cclxuIiwiI2JsIHtcbiAgYW5pbWF0aW9uOiBibGlua2VyIDNzIGxpbmVhciBpbmZpbml0ZTtcbiAgY29sb3I6ICMxYzg3Yzk7XG59XG5cbkBrZXlmcmFtZXMgYmxpbmtlciB7XG4gIDUwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxufVxuLmJsaW5rLW9uZSB7XG4gIGFuaW1hdGlvbjogYmxpbmtlci1vbmUgMXMgbGluZWFyIGluZmluaXRlO1xufVxuXG5Aa2V5ZnJhbWVzIGJsaW5rZXItb25lIHtcbiAgMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbn1cbi5ibGluay10d28ge1xuICBhbmltYXRpb246IGJsaW5rZXItdHdvIDEuNHMgbGluZWFyIGluZmluaXRlO1xufVxuXG5Aa2V5ZnJhbWVzIGJsaW5rZXItdHdvIHtcbiAgMTAwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HomeComponent.prototype, "isLogged", void 0);
HomeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")).default]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");








let HomeModule = class HomeModule {
};
HomeModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"]
        ],
        declarations: [_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"]],
        exports: [_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"]],
        providers: []
    })
], HomeModule);



/***/ }),

/***/ "./src/app/impact/impact.component.css":
/*!*********************************************!*\
  !*** ./src/app/impact/impact.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ltcGFjdC9pbXBhY3QuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/impact/impact.component.ts":
/*!********************************************!*\
  !*** ./src/app/impact/impact.component.ts ***!
  \********************************************/
/*! exports provided: ImpactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpactComponent", function() { return ImpactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ImpactComponent = class ImpactComponent {
    constructor() { }
    ngOnInit() {
    }
};
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ImpactComponent.prototype, "isLogged", void 0);
ImpactComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-impact',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./impact.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/impact/impact.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./impact.component.css */ "./src/app/impact/impact.component.css")).default]
    })
], ImpactComponent);



/***/ }),

/***/ "./src/app/landing/landing.component.scss":
/*!************************************************!*\
  !*** ./src/app/landing/landing.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ul {\n  padding: 0;\n  width: 1200px;\n  margin: 20px auto;\n}\n\nli {\n  display: inline;\n}\n\n.tn {\n  margin: 6px 6px;\n  border: 4px solid #eee;\n  box-shadow: #555 1px 1px 8px 1px;\n  cursor: pointer;\n}\n\n.modal-content {\n  width: 1200px !important;\n}\n\nh1, h2 {\n  margin-bottom: 20px;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGFuZGluZy9DOlxcUmVwb3NcXHJpZGFjXFxmcm9udGVuZC9zcmNcXGFwcFxcbGFuZGluZ1xcbGFuZGluZy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGFuZGluZy9sYW5kaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQUssVUFBQTtFQUFXLGFBQUE7RUFBYyxpQkFBQTtBQ0k5Qjs7QURIQTtFQUFLLGVBQUE7QUNPTDs7QUROQTtFQUNFLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtBQ1NGOztBRFBBO0VBQ0Usd0JBQUE7QUNVRjs7QURQQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7QUNVRiIsImZpbGUiOiJzcmMvYXBwL2xhbmRpbmcvbGFuZGluZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInVsIHsgcGFkZGluZzowOyB3aWR0aDoxMjAwcHg7IG1hcmdpbjoyMHB4IGF1dG99XHJcbmxpIHsgZGlzcGxheTppbmxpbmU7fVxyXG4udG57XHJcbiAgbWFyZ2luOjZweCA2cHg7XHJcbiAgYm9yZGVyOiA0cHggc29saWQgI2VlZTtcclxuICBib3gtc2hhZG93OiM1NTUgMXB4IDFweCA4cHggMXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlclxyXG59XHJcbi5tb2RhbC1jb250ZW50IHtcclxuICB3aWR0aDogMTIwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmgxLCBoMntcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4iLCJ1bCB7XG4gIHBhZGRpbmc6IDA7XG4gIHdpZHRoOiAxMjAwcHg7XG4gIG1hcmdpbjogMjBweCBhdXRvO1xufVxuXG5saSB7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cblxuLnRuIHtcbiAgbWFyZ2luOiA2cHggNnB4O1xuICBib3JkZXI6IDRweCBzb2xpZCAjZWVlO1xuICBib3gtc2hhZG93OiAjNTU1IDFweCAxcHggOHB4IDFweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubW9kYWwtY29udGVudCB7XG4gIHdpZHRoOiAxMjAwcHggIWltcG9ydGFudDtcbn1cblxuaDEsIGgyIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/landing/landing.component.ts":
/*!**********************************************!*\
  !*** ./src/app/landing/landing.component.ts ***!
  \**********************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/image.service */ "./src/app/services/image.service.ts");



let LandingComponent = class LandingComponent {
    constructor(imageService) {
        this.imageService = imageService;
        this.filterBy = 'all';
        this.visibleImagesMali = [];
        this.visibleImagesSenegal = [];
        this.visibleImagesMali = this.imageService.getImagesMali();
        this.visibleImagesSenegal = this.imageService.getImagesSenegal();
    }
    ngOnInit() { }
};
LandingComponent.ctorParameters = () => [
    { type: _services_image_service__WEBPACK_IMPORTED_MODULE_2__["ImageService"] }
];
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], LandingComponent.prototype, "isLogged", void 0);
LandingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-landing',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./landing.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/landing/landing.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./landing.component.scss */ "./src/app/landing/landing.component.scss")).default]
    })
], LandingComponent);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");





let LoginComponent = class LoginComponent {
    constructor(authService, route, tokenStorage) {
        this.authService = authService;
        this.route = route;
        this.tokenStorage = tokenStorage;
        this.isCollapsed = true;
        this.date = new Date();
        this.pagination = 3;
        this.pagination1 = 1;
        this.form = {};
        this.isLoggedIn = false;
        this.isLoginFailed = false;
        this.errorMessage = '';
        this.roles = [];
    }
    ngOnInit() {
        this.tokenStorage.signOut();
        this.isLogged = false;
    }
    onSubmit(form) {
        console.log(form.value);
        this.authService.attemptAuth(form.value).subscribe(data => {
            this.isLoginFailed = false;
            this.isLoggedIn = true;
            this.isLogged = true;
            //console.log(data);
            this.tokenStorage.saveToken(data.token);
            this.tokenStorage.saveFirstname(data.user.nom);
            this.tokenStorage.saveLastname(data.user.prenom);
            this.tokenStorage.saveEmail(data.user.email);
            this.tokenStorage.saveTel(data.user.telephone);
            this.tokenStorage.saveAddress(data.user.adresse);
            console.log(data);
            //console.log('Token : ', this.tokenStorage.getToken());
            this.route.navigate(['/user-profile']);
        }, error => {
            console.log(error);
            this.errorMessage = error.error.message;
            this.isLoginFailed = true;
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }
];
LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/modal/modal.component.css":
/*!*******************************************!*\
  !*** ./src/app/modal/modal.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFsL21vZGFsLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/modal/modal.component.ts":
/*!******************************************!*\
  !*** ./src/app/modal/modal.component.ts ***!
  \******************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ModalComponent = class ModalComponent {
    constructor() { }
    ngOnInit() {
    }
};
ModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./modal.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modal/modal.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./modal.component.css */ "./src/app/modal/modal.component.css")).default]
    })
], ModalComponent);



/***/ }),

/***/ "./src/app/new/new.component.css":
/*!***************************************!*\
  !*** ./src/app/new/new.component.css ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ldy9uZXcuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/new/new.component.ts":
/*!**************************************!*\
  !*** ./src/app/new/new.component.ts ***!
  \**************************************/
/*! exports provided: NewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewComponent", function() { return NewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let NewComponent = class NewComponent {
    constructor() { }
    ngOnInit() {
        var nucleoView = document.getElementsByClassName('icons-container')[0];
        window.addEventListener('scroll', function (event) {
            if (this.isInViewport(nucleoView)) {
                nucleoView.classList.add('on-screen');
            }
            else {
                nucleoView.classList.remove('on-screen');
            }
        }.bind(this), false);
    }
    isInViewport(elem) {
        var bounding = elem.getBoundingClientRect();
        return (bounding.top >= 0 &&
            bounding.left >= 0 &&
            bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            bounding.right <= (window.innerWidth || document.documentElement.clientWidth));
    }
    ;
};
NewComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-new',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/new/new.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new.component.css */ "./src/app/new/new.component.css")).default]
    })
], NewComponent);



/***/ }),

/***/ "./src/app/profile/profile.component.scss":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");





let ProfileComponent = class ProfileComponent {
    constructor(router, data, tokenStorage) {
        this.router = router;
        this.data = data;
        this.tokenStorage = tokenStorage;
        this.user = {
            firstname: null,
            lastname: null,
            email: null,
            tel: null,
            address: null
        };
    }
    ngOnInit() {
        //setInterval(() => location.reload(), 15000);
        this.data.currentLogin.subscribe(message => this.isLogged = message);
        this.data.changeStatus(true);
        this.user.firstname = this.tokenStorage.getFirsttname();
        this.user.lastname = this.tokenStorage.getLastname();
        this.user.email = this.tokenStorage.getEmail();
        this.user.tel = this.tokenStorage.getTel();
        this.user.address = this.tokenStorage.getAdress();
    }
};
ProfileComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"] },
    { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }
];
ProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.component.scss */ "./src/app/profile/profile.component.scss")).default]
    })
], ProfileComponent);



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
let AuthService = class AuthService {
    constructor(http, router) {
        this.http = http;
        this.router = router;
        this.loginUrl = 'http://localhost:8080/auth';
        this.signupUrl = 'http://localhost:8080/register';
        this.payUrl = 'http://localhost:8080/payement/payment_charge';
    }
    attemptAuth(credentials) {
        return this.http.post(this.loginUrl, credentials, httpOptions);
    }
    signUp(info) {
        return this.http.post(this.signupUrl, info, httpOptions);
    }
    pay(payInfo) {
        return this.http.post(this.payUrl, payInfo, httpOptions);
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);



/***/ }),

/***/ "./src/app/services/contact.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/contact.service.ts ***!
  \*********************************************/
/*! exports provided: ContactService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactService", function() { return ContactService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let ContactService = class ContactService {
    constructor(http) {
        this.http = http;
        this.url = 'http://localhost:8080/contact';
    }
    sendMessage(messageContent) {
        return this.http.post(this.url, JSON.stringify(messageContent), { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' }), responseType: 'text' });
    }
};
ContactService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ContactService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ContactService);



/***/ }),

/***/ "./src/app/services/data.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



let DataService = class DataService {
    constructor() {
        this.loginOrNo = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        this.currentLogin = this.loginOrNo.asObservable();
    }
    changeStatus(message) {
        this.loginOrNo.next(message);
    }
};
DataService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ "./src/app/services/image.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/image.service.ts ***!
  \*******************************************/
/*! exports provided: ImageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageService", function() { return ImageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ImageService = class ImageService {
    constructor() {
        this.visibleImagesMAli = [];
        this.visibleImagesSenegal = [];
    }
    getImagesMali() {
        return this.visibleImagesMAli = IMAGESMALI.slice(0);
    }
    getImagesSenegal() {
        return this.visibleImagesSenegal = IMAGESSENEGAL.slice(0);
    }
    getImage(id) {
        return IMAGESMALI.slice(0).find(image => image.id == id);
    }
};
ImageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImageService);

const IMAGESMALI = [
    { id: 1, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/1.jpeg' },
    { id: 2, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/2.jpeg' },
    { id: 3, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/3.jpeg' },
    { id: 4, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/4.jpeg' },
    { id: 5, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/5.jpeg' },
    { id: 6, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/6.jpeg' },
    { id: 7, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/7.jpeg' },
    { id: 8, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/8.jpeg' },
    { id: 9, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/9.jpeg' },
    { id: 10, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/10.jpeg' },
    { id: 11, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/11.jpeg' },
    { id: 12, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/12.jpeg' },
    { id: 13, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/13.jpeg' },
    { id: 14, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/14.jpeg' },
    { id: 15, category: 'dons', caption: 'mali', url: 'assets/img/images_friqya/dons/mali/15.jpeg' }
];
const IMAGESSENEGAL = [
    { id: 1, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/1.jpeg' },
    { id: 2, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/2.jpeg' },
    { id: 3, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/3.jpeg' },
    { id: 4, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/4.jpeg' },
    { id: 5, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/5.jpeg' },
    { id: 6, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/6.jpeg' },
    { id: 7, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/7.jpeg' },
    { id: 8, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/8.jpeg' },
    { id: 9, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/9.jpeg' },
    { id: 10, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/10.jpeg' },
    { id: 11, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/11.jpeg' },
    { id: 12, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/12.jpeg' },
    { id: 13, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/13.jpeg' },
    { id: 14, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/14.jpeg' },
    { id: 15, category: 'dons', caption: 'senegal', url: 'assets/img/images_friqya/dons/senegal/15.jpeg' }
];


/***/ }),

/***/ "./src/app/services/liste-pays.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/liste-pays.service.ts ***!
  \************************************************/
/*! exports provided: ListePaysService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListePaysService", function() { return ListePaysService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm2015/add/operator/map.js");




let ListePaysService = class ListePaysService {
    constructor(http) {
        this.http = http;
        this.dataarray = [];
    }
    getData() {
        return this.http.get('https://restcountries.eu/rest/v2/all').map(res => res);
    }
};
ListePaysService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ListePaysService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], ListePaysService);



/***/ }),

/***/ "./src/app/services/token-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/token-storage.service.ts ***!
  \***************************************************/
/*! exports provided: TokenStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenStorageService", function() { return TokenStorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


const TOKEN_KEY = 'AuthToken';
const FIRSTNAME_KEY = 'AuthFistname';
const EMAIL_KEY = 'AuthEmail';
const LASTNAME_KEY = 'AuthLastname';
const TEL_KEY = 'AuthTel';
const ADDRESS_KEY = 'AuthAddress';
let TokenStorageService = class TokenStorageService {
    constructor() {
        this.roles = [];
    }
    signOut() {
        window.sessionStorage.clear();
    }
    saveToken(token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }
    getToken() {
        return sessionStorage.getItem(TOKEN_KEY);
    }
    saveFirstname(firstname) {
        window.sessionStorage.removeItem(FIRSTNAME_KEY);
        window.sessionStorage.setItem(FIRSTNAME_KEY, firstname);
    }
    getFirsttname() {
        return sessionStorage.getItem(FIRSTNAME_KEY);
    }
    saveLastname(lastname) {
        window.sessionStorage.removeItem(LASTNAME_KEY);
        window.sessionStorage.setItem(LASTNAME_KEY, lastname);
    }
    getLastname() {
        return sessionStorage.getItem(LASTNAME_KEY);
    }
    saveEmail(email) {
        window.sessionStorage.removeItem(EMAIL_KEY);
        window.sessionStorage.setItem(EMAIL_KEY, email);
    }
    getEmail() {
        return sessionStorage.getItem(EMAIL_KEY);
    }
    saveTel(tel) {
        window.sessionStorage.removeItem(TEL_KEY);
        window.sessionStorage.setItem(TEL_KEY, tel);
    }
    getTel() {
        return sessionStorage.getItem(TEL_KEY);
    }
    saveAddress(address) {
        window.sessionStorage.removeItem(ADDRESS_KEY);
        window.sessionStorage.setItem(ADDRESS_KEY, address);
    }
    getAdress() {
        return sessionStorage.getItem(ADDRESS_KEY);
    }
};
TokenStorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], TokenStorageService);



/***/ }),

/***/ "./src/app/shared/footer/footer.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/shared/footer/footer.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let FooterComponent = class FooterComponent {
    constructor(router) {
        this.router = router;
        this.test = new Date();
    }
    ngOnInit() {
    }
    getPath() {
        return this.router.url;
    }
};
FooterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
FooterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/footer/footer.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./footer.component.scss */ "./src/app/shared/footer/footer.component.scss")).default]
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#bl {\n  -webkit-animation: blinker 3s linear infinite;\n          animation: blinker 3s linear infinite;\n  color: #1c87c9;\n}\n\n@-webkit-keyframes blinker {\n  50% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker {\n  50% {\n    opacity: 0;\n  }\n}\n\n.blink-one {\n  -webkit-animation: blinker-one 1s linear infinite;\n          animation: blinker-one 1s linear infinite;\n}\n\n@-webkit-keyframes blinker-one {\n  0% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker-one {\n  0% {\n    opacity: 0;\n  }\n}\n\n.blink-two {\n  -webkit-animation: blinker-two 1.4s linear infinite;\n          animation: blinker-two 1.4s linear infinite;\n}\n\n@-webkit-keyframes blinker-two {\n  100% {\n    opacity: 0;\n  }\n}\n\n@keyframes blinker-two {\n  100% {\n    opacity: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL25hdmJhci9DOlxcUmVwb3NcXHJpZGFjXFxmcm9udGVuZC9zcmNcXGFwcFxcc2hhcmVkXFxuYXZiYXJcXG5hdmJhci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2Q0FBQTtVQUFBLHFDQUFBO0VBQ0EsY0FBQTtBQ0NGOztBRENBO0VBQ0U7SUFBTSxVQUFBO0VDR047QUFDRjs7QURMQTtFQUNFO0lBQU0sVUFBQTtFQ0dOO0FBQ0Y7O0FERkE7RUFDRSxpREFBQTtVQUFBLHlDQUFBO0FDSUY7O0FERkE7RUFDRTtJQUFLLFVBQUE7RUNNTDtBQUNGOztBRFJBO0VBQ0U7SUFBSyxVQUFBO0VDTUw7QUFDRjs7QURMQTtFQUNFLG1EQUFBO1VBQUEsMkNBQUE7QUNPRjs7QURMQTtFQUNFO0lBQU8sVUFBQTtFQ1NQO0FBQ0Y7O0FEWEE7RUFDRTtJQUFPLFVBQUE7RUNTUDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYmwge1xyXG4gIGFuaW1hdGlvbjogYmxpbmtlciAzcyBsaW5lYXIgaW5maW5pdGU7XHJcbiAgY29sb3I6ICMxYzg3Yzk7XHJcbn1cclxuQGtleWZyYW1lcyBibGlua2VyIHtcclxuICA1MCUgeyBvcGFjaXR5OiAwOyB9XHJcbn1cclxuLmJsaW5rLW9uZSB7XHJcbiAgYW5pbWF0aW9uOiBibGlua2VyLW9uZSAxcyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuQGtleWZyYW1lcyBibGlua2VyLW9uZSB7XHJcbiAgMCUgeyBvcGFjaXR5OiAwOyB9XHJcbn1cclxuLmJsaW5rLXR3byB7XHJcbiAgYW5pbWF0aW9uOiBibGlua2VyLXR3byAxLjRzIGxpbmVhciBpbmZpbml0ZTtcclxufVxyXG5Aa2V5ZnJhbWVzIGJsaW5rZXItdHdvIHtcclxuICAxMDAlIHsgb3BhY2l0eTogMDsgfVxyXG59XHJcbiIsIiNibCB7XG4gIGFuaW1hdGlvbjogYmxpbmtlciAzcyBsaW5lYXIgaW5maW5pdGU7XG4gIGNvbG9yOiAjMWM4N2M5O1xufVxuXG5Aa2V5ZnJhbWVzIGJsaW5rZXIge1xuICA1MCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbn1cbi5ibGluay1vbmUge1xuICBhbmltYXRpb246IGJsaW5rZXItb25lIDFzIGxpbmVhciBpbmZpbml0ZTtcbn1cblxuQGtleWZyYW1lcyBibGlua2VyLW9uZSB7XG4gIDAlIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG59XG4uYmxpbmstdHdvIHtcbiAgYW5pbWF0aW9uOiBibGlua2VyLXR3byAxLjRzIGxpbmVhciBpbmZpbml0ZTtcbn1cblxuQGtleWZyYW1lcyBibGlua2VyLXR3byB7XG4gIDEwMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/data.service */ "./src/app/services/data.service.ts");






let NavbarComponent = class NavbarComponent {
    constructor(location, router, tokenStorage, data) {
        this.location = location;
        this.router = router;
        this.tokenStorage = tokenStorage;
        this.data = data;
        this.isCollapsed = true;
        this.yScrollStack = [];
    }
    ngOnInit() {
        this.data.currentLogin.subscribe(message => { this.isLogged = message; });
        this.router.events.subscribe((event) => {
            this.isCollapsed = true;
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (event.url != this.lastPoppedUrl)
                    this.yScrollStack.push(window.scrollY);
            }
            else if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                if (event.url == this.lastPoppedUrl) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo(0, this.yScrollStack.pop());
                }
                else
                    window.scrollTo(0, 0);
            }
        });
        this.location.subscribe((ev) => {
            this.lastPoppedUrl = ev.url;
        });
    }
    isHome() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee === '#/home') {
            return true;
        }
        else {
            return false;
        }
    }
    isDocumentation() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee === '#/documentation') {
            return true;
        }
        else {
            return false;
        }
    }
    logout() {
        this.tokenStorage.signOut();
        this.isLogged = false;
        this.data.changeStatus(false);
        this.router.navigate(['/home']);
    }
};
NavbarComponent.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] },
    { type: _services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] }
];
NavbarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navbar',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/navbar/navbar.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./navbar.component.scss */ "./src/app/shared/navbar/navbar.component.scss")).default]
    })
], NavbarComponent);



/***/ }),

/***/ "./src/app/signup/signup.component.scss":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9zaWdudXAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let SignupComponent = class SignupComponent {
    constructor(authService, route) {
        this.authService = authService;
        this.route = route;
        this.isCollapsed = true;
        this.date = new Date();
        this.pagination = 3;
        this.pagination1 = 1;
        this.form = {};
        this.errorMessage = '';
        this.roles = [];
        this.isInscription = false;
        this.successful = false;
    }
    onSubmit(form) {
        console.log(form.value);
        this.authService.attemptAuth(form.value).subscribe(data => {
            this.successful = true;
            this.isInscription = false;
            this.route.navigate(['/login']);
        }, error => {
            console.log(error);
            this.errorMessage = error.error.message;
            this.isInscription = true;
        });
    }
};
SignupComponent.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SignupComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-signup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./signup.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./signup.component.scss */ "./src/app/signup/signup.component.scss")).default]
    })
], SignupComponent);



/***/ }),

/***/ "./src/app/strip-payement/strip-payement.component.css":
/*!*************************************************************!*\
  !*** ./src/app/strip-payement/strip-payement.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("form.checkout {\r\n  max-width: 500px;\r\n  margin: 2rem auto;\r\n  text-align: center;\r\n  border: 2px solid #eee;\r\n  border-radius: 8px;\r\n  padding: 1rem 2rem;\r\n  background: white;\r\n\r\n  font-family: monospace;\r\n  color: #525252;\r\n  font-size: 1.1rem;\r\n}\r\n\r\nform.checkout button {\r\n  padding: 0.5rem 1rem;\r\n  color: white;\r\n  background: coral;\r\n  border: none;\r\n  border-radius: 4px;\r\n  margin-top: 1rem;\r\n}\r\n\r\nform.checkout button:active {\r\n  background: rgb(165, 76, 43);\r\n}\r\n\r\n.StripeElement {\r\n  margin: 1rem 0 1rem;\r\n  background-color: white;\r\n  padding: 8px 12px;\r\n  border-radius: 4px;\r\n  border: 1px solid transparent;\r\n  box-shadow: 0 1px 3px 0 #e6ebf1;\r\n  transition: box-shadow 150ms ease;\r\n}\r\n\r\n.StripeElement--focus {\r\n  box-shadow: 0 1px 3px 0 #cfd7df;\r\n}\r\n\r\n.StripeElement--invalid {\r\n  border-color: #fa755a;\r\n}\r\n\r\n.StripeElement--webkit-autofill {\r\n  background-color: #fefde5 !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3RyaXAtcGF5ZW1lbnQvc3RyaXAtcGF5ZW1lbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjs7RUFFakIsc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLDRCQUE0QjtBQUM5Qjs7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IsK0JBQStCO0VBRS9CLGlDQUFpQztBQUNuQzs7QUFFQTtFQUNFLCtCQUErQjtBQUNqQzs7QUFFQTtFQUNFLHFCQUFxQjtBQUN2Qjs7QUFFQTtFQUNFLG9DQUFvQztBQUN0QyIsImZpbGUiOiJzcmMvYXBwL3N0cmlwLXBheWVtZW50L3N0cmlwLXBheWVtZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb3JtLmNoZWNrb3V0IHtcclxuICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gIG1hcmdpbjogMnJlbSBhdXRvO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjZWVlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBwYWRkaW5nOiAxcmVtIDJyZW07XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcblxyXG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7XHJcbiAgY29sb3I6ICM1MjUyNTI7XHJcbiAgZm9udC1zaXplOiAxLjFyZW07XHJcbn1cclxuXHJcbmZvcm0uY2hlY2tvdXQgYnV0dG9uIHtcclxuICBwYWRkaW5nOiAwLjVyZW0gMXJlbTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZDogY29yYWw7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG59XHJcblxyXG5mb3JtLmNoZWNrb3V0IGJ1dHRvbjphY3RpdmUge1xyXG4gIGJhY2tncm91bmQ6IHJnYigxNjUsIDc2LCA0Myk7XHJcbn1cclxuLlN0cmlwZUVsZW1lbnQge1xyXG4gIG1hcmdpbjogMXJlbSAwIDFyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgcGFkZGluZzogOHB4IDEycHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDNweCAwICNlNmViZjE7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDE1MG1zIGVhc2U7XHJcbiAgdHJhbnNpdGlvbjogYm94LXNoYWRvdyAxNTBtcyBlYXNlO1xyXG59XHJcblxyXG4uU3RyaXBlRWxlbWVudC0tZm9jdXMge1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDNweCAwICNjZmQ3ZGY7XHJcbn1cclxuXHJcbi5TdHJpcGVFbGVtZW50LS1pbnZhbGlkIHtcclxuICBib3JkZXItY29sb3I6ICNmYTc1NWE7XHJcbn1cclxuXHJcbi5TdHJpcGVFbGVtZW50LS13ZWJraXQtYXV0b2ZpbGwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZkZTUgIWltcG9ydGFudDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/strip-payement/strip-payement.component.ts":
/*!************************************************************!*\
  !*** ./src/app/strip-payement/strip-payement.component.ts ***!
  \************************************************************/
/*! exports provided: StripPayementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StripPayementComponent", function() { return StripPayementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");



let StripPayementComponent = class StripPayementComponent {
    constructor(cd, authService) {
        this.cd = cd;
        this.authService = authService;
        this.cardHandler = this.onChange.bind(this);
    }
    ngAfterViewInit() {
        const style = {
            base: {
                lineHeight: '24px',
                fontFamily: 'monospace',
                fontSmoothing: 'antialiased',
                fontSize: '19px',
                '::placeholder': {
                    color: 'purple'
                }
            }
        };
        console.log(this.cardInfo);
        this.card = elements.create('card');
        this.card.mount(this.cardInfo.nativeElement);
        this.card.addEventListener('change', this.cardHandler);
    }
    ngOnDestroy() {
        this.card.removeEventListener('change', this.cardHandler);
        this.card.destroy();
    }
    onChange({ error }) {
        if (error) {
            this.error = error.message;
        }
        else {
            this.error = null;
        }
        this.cd.detectChanges();
    }
    onSubmit(form) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const { token, error } = yield stripe.createToken(this.card);
            if (error) {
                console.log('Something is wrong:', error);
            }
            else {
                console.log('Success!', token);
                // ...send the token to the your backend to process the charge
                this.authService.pay(token).subscribe(data => {
                    console.log(data);
                }, error => {
                    console.log(error);
                });
            }
        });
    }
};
StripPayementComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('cardInfo')
], StripPayementComponent.prototype, "cardInfo", void 0);
StripPayementComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-strip-payement',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./strip-payement.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/strip-payement/strip-payement.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./strip-payement.component.css */ "./src/app/strip-payement/strip-payement.component.css")).default]
    })
], StripPayementComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Repos\ridac\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map