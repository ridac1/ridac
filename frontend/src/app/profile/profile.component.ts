import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../services/data.service';
import {TokenStorageService} from '../services/token-storage.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

user = {
  firstname : null,
  lastname :null,
  email : null,
  tel : null,
  address : null
};
  isLogged;
    constructor( private router: Router, private data: DataService, private tokenStorage: TokenStorageService) { }

    ngOnInit() {

      //setInterval(() => location.reload(), 15000);
      this.data.currentLogin.subscribe(message => this.isLogged = message);
      this.data.changeStatus(true);

      this.user.firstname = this.tokenStorage.getFirsttname();
      this.user.lastname = this.tokenStorage.getLastname();
      this.user.email = this.tokenStorage.getEmail();
      this.user.tel = this.tokenStorage.getTel();
      this.user.address = this.tokenStorage.getAdress();

    }

}
