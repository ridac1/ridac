import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {AuthSignUpInfo} from '../models/signup';
import {Router} from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;

  form: any = {};

  errorMessage = '';
  roles: string[] = [];
  private signUpInfo: AuthSignUpInfo;
  board: string;
  isInscription =false;
  successful = false;

    constructor(private authService: AuthService, private route:Router) { }

  onSubmit(form){

    console.log(form.value)

    this.authService.attemptAuth(form.value).subscribe(
      data => {
        this.successful=true;
        this.isInscription = false;
        this.route.navigate(['/login']);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isInscription = true;
      }
    );
  }
}
