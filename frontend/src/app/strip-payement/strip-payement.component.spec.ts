import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StripPayementComponent } from './strip-payement.component';

describe('StripPayementComponent', () => {
  let component: StripPayementComponent;
  let fixture: ComponentFixture<StripPayementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StripPayementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StripPayementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
