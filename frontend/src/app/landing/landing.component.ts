import {Component, Input, OnInit} from '@angular/core';
import {ImageService} from '../services/image.service';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
  focus: any;
  focus1: any;
  @Input() isLogged;
  images:any[];
  filterBy?: string = 'all'
  visibleImagesMali:any[] = [];
  visibleImagesSenegal:any[] = [];

  constructor(private imageService: ImageService) {
    this.visibleImagesMali = this.imageService.getImagesMali();
    this.visibleImagesSenegal = this.imageService.getImagesSenegal();
  }

  ngOnInit() {}


}
