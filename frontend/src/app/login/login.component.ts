import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthLoginInfo} from '../models/login';
import {AuthService} from '../services/auth.service';
import {TokenStorageService} from '../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  private loginInfo: AuthLoginInfo;
  board: string;

 isLogged;
  constructor(private authService: AuthService, private route:Router, private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    this.tokenStorage.signOut();
    this.isLogged = false;
  }

  onSubmit(form){

      console.log(form.value)

      this.authService.attemptAuth(form.value).subscribe(
        data => {
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          this.isLogged = true;
          //console.log(data);
          this.tokenStorage.saveToken(data.token);
          this.tokenStorage.saveFirstname(data.user.nom);
          this.tokenStorage.saveLastname(data.user.prenom);
          this.tokenStorage.saveEmail(data.user.email);
          this.tokenStorage.saveTel(data.user.telephone);
          this.tokenStorage.saveAddress(data.user.adresse)
          console.log(data);
          //console.log('Token : ', this.tokenStorage.getToken());
          this.route.navigate(['/user-profile']);
        },
        error => {
          console.log(error);
          this.errorMessage = error.error.message;
          this.isLoginFailed = true;
        }
      );
    }
}
