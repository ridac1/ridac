import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-impact',
  templateUrl: './impact.component.html',
  styleUrls: ['./impact.component.css']
})
export class ImpactComponent implements OnInit {

  @Input() isLogged;
  constructor() { }

  ngOnInit(): void {
  }

}
