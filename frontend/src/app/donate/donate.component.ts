import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {DonateInfo} from '../models/donate';
import {Router} from '@angular/router';
import {AuthSignUpInfo} from '../models/signup';
import { ListePaysService} from '../services/liste-pays.service';
import {NgForm} from '@angular/forms';
import {PayInfo} from '../models/pay-info';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css'],
  providers:  [ListePaysService]
})
export class DonateComponent implements OnInit, AfterViewInit, OnDestroy{
  focus;
  date = new Date();
  data : any;
  form: any = {};

  payInfo = {
    type : null,
  nom :null,
  prenom :null,
  email :null,
  pays :null,
  tel :null,
  sexe :null,
  montant :null,
  devise : null,
  cardTocken : null,
  mot : null
  };

  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(private authService: AuthService, private route:Router,private  listePaysService:ListePaysService, private cd: ChangeDetectorRef) { }

  ngOnInit(){
    this.getData()
  }

  getData(){
    this.listePaysService.getData().subscribe(data => {
      this.data = data;
      console.log(this.data);
    });

  }

  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'purple'
        }
      }
    };

    console.log(this.cardInfo);
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({error}) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {


    const {token, error} = await stripe.createToken(this.card);

    this.payInfo.nom = form.value.nom;
    this.payInfo.prenom = form.value.prenom;
    this.payInfo.type = form.value.type;
    this.payInfo.email = form.value.email;
    this.payInfo.tel = form.value.tel;
    this.payInfo.sexe = form.value.sexe;
    this.payInfo.pays = form.value.country;
    this.payInfo.montant = form.value.mnt;
    this.payInfo.devise = form.value.devise;
    this.payInfo.mot = form.value.comment;

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      // ...send the token to the your backend to process the charge
      this.payInfo.cardTocken = token;
      this.authService.pay(this.payInfo).subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
        }
      );


    }
  }
}
