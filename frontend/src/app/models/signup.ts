
export class AuthSignUpInfo{
  nom: string;
  prenom: string;
  sexe: string;
  email: string;
  telephone: number;
  adresse: string;
  password: string;


  constructor(nom: string, prenom: string, sexe: string, email: string, telephone: number, adresse: string, password: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.sexe = sexe;
    this.email = email;
    this.telephone = telephone;
    this.adresse = adresse;
    this.password = password;
  }
}
