export class PayInfo {

  type : string;
  nom : string;
  prenom : string;
  email : string;
  pays : string;
  tel : string;
  sexe : string;
  montant : number;
  devis : string;
  cardTocken : any;
  mot : string;

  constructor(type: string, nom: string, prenom: string, email: string, pays: string, tel: string, sexe: string, montant: number, devis: string, cardTocken: any, mot: string) {
    this.type = type;
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.pays = pays;
    this.tel = tel;
    this.sexe = sexe;
    this.montant = montant;
    this.devis = devis;
    this.cardTocken = cardTocken;
    this.mot = mot;
  }
}
