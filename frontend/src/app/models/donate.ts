export class DonateInfo {
  nom: string;
  prenom: string;
  sexe: string;
  email: string;
  pays: string;
  comment: string;
  modePaiement: string;
  telephone: number;
  montantDon: number;
  numeroCarte: string;

  constructor(nom: string, prenom: string, sexe: string, email: string, pays: string, comment: string, modePaiement: string, telephone: number, montantDon: number, numeroCarte: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.sexe = sexe;
    this.email = email;
    this.pays = pays;
    this.comment = comment;
    this.modePaiement = modePaiement;
    this.telephone = telephone;
    this.montantDon = montantDon;
    this.numeroCarte = numeroCarte;
  }

}
