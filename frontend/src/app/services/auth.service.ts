import { Injectable } from '@angular/core';
import {AuthLoginInfo} from '../models/login';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthSignUpInfo} from '../models/signup';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl = 'http://localhost:8080/auth';
  private signupUrl = 'http://localhost:8080/register';
  private payUrl = 'http://localhost:8080/payement/payment_charge';

  constructor(private http: HttpClient,private router:Router) {

  }

  attemptAuth(credentials: AuthLoginInfo): Observable<any> {
    return this.http.post(this.loginUrl, credentials, httpOptions);
  }

  signUp(info: AuthSignUpInfo): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions);
  }

  pay(payInfo): Observable<any> {
    return this.http.post(this.payUrl, payInfo, httpOptions);
  }
}
