import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private loginOrNo = new BehaviorSubject(false);
  currentLogin = this.loginOrNo.asObservable();

  constructor() { }

  changeStatus(message: boolean) {
    this.loginOrNo.next(message)
  }
}



