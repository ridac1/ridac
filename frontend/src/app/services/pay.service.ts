import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthLoginInfo} from '../models/login';
import {Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class PayService {

  private payUrl = 'http://localhost:8080/auth';

  constructor(private http: HttpClient,private router:Router) {

  }

  doPay(credentials): Observable<any> {
    return this.http.post(this.payUrl, credentials, httpOptions);
  }

}
