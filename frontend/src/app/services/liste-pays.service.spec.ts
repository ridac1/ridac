import { TestBed } from '@angular/core/testing';

import { ListePaysService } from './liste-pays.service';

describe('ListePaysService', () => {
  let service: ListePaysService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListePaysService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
