import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map'



@Injectable()

export class ListePaysService {
  dataarray = [];

  constructor(private http: HttpClient) {
  }

  getData() {
    return this.http.get('https://restcountries.eu/rest/v2/all').map(res => res);
  }
}
