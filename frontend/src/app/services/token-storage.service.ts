import { Injectable } from '@angular/core';
const TOKEN_KEY = 'AuthToken';
const FIRSTNAME_KEY = 'AuthFistname';
const EMAIL_KEY = 'AuthEmail';
const LASTNAME_KEY = 'AuthLastname';
const TEL_KEY = 'AuthTel';
const ADDRESS_KEY = 'AuthAddress';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private roles: Array<string> = [];
  constructor() { }

  signOut() {
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveFirstname(firstname: string) {
    window.sessionStorage.removeItem(FIRSTNAME_KEY);
    window.sessionStorage.setItem(FIRSTNAME_KEY, firstname);
  }

  public getFirsttname(): string {
    return sessionStorage.getItem(FIRSTNAME_KEY);
  }

  public saveLastname(lastname: string) {
    window.sessionStorage.removeItem(LASTNAME_KEY);
    window.sessionStorage.setItem(LASTNAME_KEY, lastname);
  }

  public getLastname(): string {
    return sessionStorage.getItem(LASTNAME_KEY);
  }


  public saveEmail(email: string) {
    window.sessionStorage.removeItem(EMAIL_KEY);
    window.sessionStorage.setItem(EMAIL_KEY, email);
  }

  public getEmail(): string {
    return sessionStorage.getItem(EMAIL_KEY);
  }

  public saveTel(tel: string) {
    window.sessionStorage.removeItem(TEL_KEY);
    window.sessionStorage.setItem(TEL_KEY, tel);
  }

  public getTel(): string {
    return sessionStorage.getItem(TEL_KEY);
  }

  public saveAddress(address: string) {
    window.sessionStorage.removeItem(ADDRESS_KEY);
    window.sessionStorage.setItem(ADDRESS_KEY, address);
  }

  public getAdress(): string {
    return sessionStorage.getItem(ADDRESS_KEY);
  }
}
