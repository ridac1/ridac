import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  visibleImagesMAli = [];
  visibleImagesSenegal = [];

  getImagesMali(){
    return this.visibleImagesMAli = IMAGESMALI.slice(0);
  }

  getImagesSenegal(){
    return this.visibleImagesSenegal = IMAGESSENEGAL.slice(0);
  }
  getImage(id: number){
    return IMAGESMALI.slice(0).find(image => image.id == id)
  }

  constructor() { }
}

const IMAGESMALI =[
  {id:1, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/1.jpeg'},
  {id:2, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/2.jpeg'},
  {id:3, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/3.jpeg'},
  {id:4, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/4.jpeg'},
  {id:5, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/5.jpeg'},
  {id:6, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/6.jpeg'},
  {id:7, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/7.jpeg'},
  {id:8, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/8.jpeg'},
  {id:9, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/9.jpeg'},
  {id:10, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/10.jpeg'},
  {id:11, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/11.jpeg'},
  {id:12, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/12.jpeg'},
  {id:13, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/13.jpeg'},
  {id:14, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/14.jpeg'},
  {id:15, category: 'dons', caption: 'mali', url:'assets/img/images_friqya/dons/mali/15.jpeg'}
]



const IMAGESSENEGAL =[
  {id:1, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/1.jpeg'},
  {id:2, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/2.jpeg'},
  {id:3, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/3.jpeg'},
  {id:4, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/4.jpeg'},
  {id:5, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/5.jpeg'},
  {id:6, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/6.jpeg'},
  {id:7, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/7.jpeg'},
  {id:8, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/8.jpeg'},
  {id:9, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/9.jpeg'},
  {id:10, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/10.jpeg'},
  {id:11, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/11.jpeg'},
  {id:12, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/12.jpeg'},
  {id:13, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/13.jpeg'},
  {id:14, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/14.jpeg'},
  {id:15, category: 'dons', caption: 'senegal', url:'assets/img/images_friqya/dons/senegal/15.jpeg'}
]
