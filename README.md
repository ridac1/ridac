# Projet Ridac

Ce projet est composé de deux applications : le frontend en Angular et le backend en Node.JS et Express

# Recupération et installation

En faisant le git clone du projet vous aurez les le frontend et le backend. Seul la branche master sera récupéré par le git clone.
Une fois le projet cloné, taper la commande suivante dans le repertoire racine du projet :

git checkout -b develop origin/develop

Avec cette commande, vous récupérerez la branche develop, à partir de laqualle vous créerez les futures branches pour les fonctionnalité à développer.

Par la suite :
- Se placer dans le repertoire frontend, executer la commande : npm install
- Faire de même dans le repertoire backend
- En cas de probleme, n'hésiter pas à contacter Bane
